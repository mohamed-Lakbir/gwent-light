package Agent.RandomAgent;

import enums.CardType;
import enums.PassOrPlaceCard;
import enums.NextAction;
import enums.Raw;
import Agent.BotAgent;
import java.util.Random;

public class DumbRandomAgent extends BotAgent {

    public DumbRandomAgent() {
        super("DumbRandomAgent");
    }


    @Override
    public NextAction calculateNextAction() {
        while (true) {
            switch (new Random().nextInt(5)) {
                case 0:
                    if(getObservedEnv().getAgentHand().containsBuffCard()){
                        return NextAction.BUFFACTION;
                    }
                    continue;
                case 1:
                    if(getObservedEnv().getAgentHand().containsDeBuffCard()){
                        return NextAction.DEBUFFACTION;
                    }
                    continue;
                case 2:
                    if(getObservedEnv().getAgentHand().containsEraseCard()){
                        return NextAction.ERASEACTION;
                    }
                    continue;
                case 3:
                    if(getObservedEnv().getAgentHand().containsMonsterCard()){
                        return NextAction.MONSTERACTION;
                    }
                    continue;
                case 4:
                    if(getObservedEnv().getAgentHand().containsPullMoreCard()){
                        return NextAction.PULLMOREACTION;
                    }
                case 5:
                    if(getObservedEnv().getAgentHand().containsAntiDebuffCard()){
                        return NextAction.ANTIDEBUFFACTION;
                    }

                    continue;
            }
            break;
        }
        return null;
    }

    @Override
    public PassOrPlaceCard PassOrTurn() {
        switch (new Random().nextInt(2)){
            case 0:
                return PassOrPlaceCard.Pass;
            case 1:
                return PassOrPlaceCard.PlaceCard;
        }
        return null;
    }

    @Override
    public String calculateNextMonsterCard() {
        return getRandomMonsterCard();
    }


    public String getRandomMonsterCard(){
        int rand;
        String cardName;
        do{
            rand = new Random().nextInt(getObservedEnv().getAgentHand().getCards().size());
            cardName = getObservedEnv().getAgentHand().getCards().get(rand).getName();
        }
        while (getObservedEnv().getAgentHand().getCards().get(rand).getCartType()!=CardType.MONSTERCARD);

        return cardName;

    }


    public Raw calculateRaw(NextAction cardType){
        Raw raw = Raw.RAW_ONE;

        switch (new Random().nextInt(3)){
            case 1:
                return raw = Raw.RAW_ONE;
            case 0:
                return raw = Raw.RAW_TWO;
            case 2:
                return raw = Raw.RAW_THREE;
        }
        return null;
    }

    @Override
    public void setPlayerNumber(String player) {
        super.setPlayerNumber(player);
    }


}
