package Agent.RuleAgent.Dumb;

import Enviroment.Model.Card.Card;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import enums.CardType;
import enums.PassOrPlaceCard;
import enums.NextAction;
import enums.Raw;
import Agent.BotAgent;

import static enums.NextAction.DEBUFFACTION;

public class DumbRuleAgentWithMagicCards extends BotAgent {

    public DumbRuleAgentWithMagicCards() {
        super("DumbRuleAgentWithMagicCards");
    }



    @Override
    public NextAction calculateNextAction() {

        for(Card c : getObservedEnv().getAgentHand().getCards()){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                return NextAction.MONSTERACTION;
            }
            if(c.getCartType().equals(CardType.PULLMORECARD)){
                return NextAction.PULLMOREACTION;
            }
            if(c.getCartType().equals(CardType.ERASECARD)){
                return NextAction.ERASEACTION;
            }

            if(c.getCartType().equals(CardType.BUFFCARD)){
                return NextAction.BUFFACTION;
            }

            if(c.getCartType().equals(CardType.DEBUFFCARD)){
                return DEBUFFACTION;
            }
            if(c.getCartType().equals(CardType.ANTIDEBUFFCARD)){
                return NextAction.ANTIDEBUFFACTION;
            }
        }
        return null;
    }

    @Override
    public PassOrPlaceCard PassOrTurn() {
        if(getObservedEnv().getAgentFieldValue()< getObservedEnv().getOpponentFieldValue()&&getObservedEnv().isOpponentPass()){
            return PassOrPlaceCard.Pass;
        }else {
            return PassOrPlaceCard.PlaceCard;
        }
    }

    @Override
    public String calculateNextMonsterCard() {
        MonsterCard cms = null;
        for(Card c : getObservedEnv().getAgentHand().getCards()){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                if(cms ==null){
                    cms = (MonsterCard) c;
                }
                MonsterCard cm = (MonsterCard) c;
                if(cms.getValue()>cm.getValue()){
                    cms = cm;
                }
            }

        }
        return cms.getName();
    }

    @Override
    public Raw calculateRaw(NextAction cardType) {
        int bestRaw =0;

        switch (cardType){
            case BUFFACTION:
                for(int i =0;i<getObservedEnv().getAgentRawValue().length;i++){
                    if(getObservedEnv().getAgentRawValue()[i]>getObservedEnv().getAgentRawValue()[bestRaw]){
                        bestRaw =i;
                    }
                }
                return getRaw(bestRaw);
            case DEBUFFACTION:
                for(int i =0;i<getObservedEnv().getAgentRawValue().length;i++){
                    if(getObservedEnv().getAgentRawValue()[i]<getObservedEnv().getAgentRawValue()[bestRaw]){
                        bestRaw =i;
                    }
                }
                return getRaw(bestRaw);
        }

        return null;
    }


    @Override
    public void setPlayerNumber(String player) {
        super.setPlayerNumber(player);
    }
}
