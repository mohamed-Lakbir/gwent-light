package Agent.RuleAgent.Dumb;

import Enviroment.Model.Card.Card;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import enums.CardType;
import enums.PassOrPlaceCard;
import enums.NextAction;
import Agent.BotAgent;
import enums.Raw;

public class DumbRuleAgent extends BotAgent {

    public DumbRuleAgent() {
        super("DumbRuleAgent");
    }


    @Override
    public NextAction calculateNextAction() {
        return NextAction.MONSTERACTION;
    }

    @Override
    public PassOrPlaceCard PassOrTurn() {
        boolean s = false;
        for(Card c : getObservedEnv().getAgentHand().getCards()){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                s = true;
            }
        }
        if(s ==false){
            return PassOrPlaceCard.Pass;
        }
        if(getObservedEnv().getAgentFieldValue()< getObservedEnv().getOpponentFieldValue()&&getObservedEnv().isOpponentPass()){
            return PassOrPlaceCard.Pass;
        }else {
            return PassOrPlaceCard.PlaceCard;
        }
    }

    @Override
    public String calculateNextMonsterCard() {
        MonsterCard cms = null;
        for(Card c : getObservedEnv().getAgentHand().getCards()){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                if(cms ==null){
                    cms = (MonsterCard) c;
                }
                MonsterCard cm = (MonsterCard) c;
                if(cms.getValue()>cm.getValue()){
                    cms = cm;
                }
            }

        }
       return cms.getName();
    }

    @Override
    public Raw calculateRaw(NextAction cardType) {


        return null;
    }


    @Override
    public void setPlayerNumber(String player) {
        super.setPlayerNumber(player);
    }
}
