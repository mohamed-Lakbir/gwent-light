package Agent.RuleAgent.CardBasedAgent;

import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import enums.PassOrPlaceCard;
import enums.NextAction;
import enums.Raw;
import Agent.*;
import static enums.NextAction.*;

public class SmartCardHoldOutAgent  extends BotAgent {
    private int moves;

    public SmartCardHoldOutAgent() {
        super("SmarterHoldOutAgent");
    }


    @Override
    public NextAction calculateNextAction() {
        if(getObservedEnv().getAgentHand().containsPullMoreCard()){
            return NextAction.PULLMOREACTION;
        }
        if(!getObservedEnv().getAgentHand().anyMonsterCard()&&getObservedEnv().getAgentHand().containsBuffCard()&&getObservedEnv().getGames()>0){
            return NextAction.BUFFACTION;
        }
        if(!getObservedEnv().getAgentHand().anyMonsterCard()&&getObservedEnv().getAgentHand().containsBuffCard()&&getObservedEnv().getGames()>0){
            return DEBUFFACTION;
        }
        if(!getObservedEnv().getAgentHand().anyMonsterCard()
                &&!getObservedEnv().getAgentHand().anyBuffCard()
                &&!getObservedEnv().getAgentHand().anyDebuffCard()){
            return ERASEACTION;
        }
        if(getObservedEnv().getAgentHand().anyMonsterCard()){
            return MONSTERACTION;
        }else{
            if(getObservedEnv().getAgentHand().containsBuffCard()){
                return NextAction.BUFFACTION;
            }
            if(getObservedEnv().getAgentHand().containsDeBuffCard()){
                return DEBUFFACTION;
            }
            if(getObservedEnv().getAgentHand().containsEraseCard()){
                return ERASEACTION;
            }
        }
        return null;
    }


    public boolean everyRawDebuffed(){
        for(boolean raw : getObservedEnv().getAgentRawDebuffed()){
            if(!raw){
                return false;
            }
        }
        return true;
    }

    @Override
    public PassOrPlaceCard PassOrTurn() {
        if(getObservedEnv().getGames() ==0){
            moves++;
        }
        if(!getObservedEnv().getAgentHand().containsPullMoreCard()&&moves<4&&getObservedEnv().getGames()==0
                ||getObservedEnv().getOpponentFieldValue()<getObservedEnv().getAgentFieldValue()&&getObservedEnv().isOpponentPass()){
            return PassOrPlaceCard.Pass;
        }
        if(everyRawDebuffed() && getObservedEnv().getAgentWins()>getObservedEnv().getOpponentWins()
                ||!getObservedEnv().getAgentHand().containsPullMoreCard()&&moves<4
                ||getObservedEnv().getOpponentFieldValue()<getObservedEnv().getAgentFieldValue()&&getObservedEnv().isOpponentPass()){
            return PassOrPlaceCard.Pass;
        }
        return PassOrPlaceCard.PlaceCard;
    }

    @Override
    public String calculateNextMonsterCard() {
        CardStack temp;
        if(getObservedEnv().getGames()==0){
            return getObservedEnv().getAgentHand().getLowestCardName();
        }
        if(everyRawDebuffed()){
            return getObservedEnv().getAgentHand().getLowestCardName();
        }
        temp = CardStack.deepCopy(getObservedEnv().getAgentHand());
        while (temp.containsMonsterCard()){

            if(checkForCardRawWithNoDebuff(temp.getLowestCard())){
                return temp.getLowestCardName();
            }
            temp.removeCard(temp.getLowestCardName());
        }
        return getObservedEnv().getAgentHand().getLowestCardName();
    }

    public boolean checkForCardRawWithNoDebuff(MonsterCard c){
        return getObservedEnv().getTable().getOwnField(getName()).checkIfRawIsDebuffes(c.getRaw());
    }



    @Override
    public Raw calculateRaw(NextAction cardType) {
        int bestRaw =0;
        switch (cardType){
            case BUFFACTION:
                for(int i =0;i<getObservedEnv().getAgentRawValue().length;i++){
                    if(getObservedEnv().getAgentRawValue()[i]>getObservedEnv().getAgentRawValue()[bestRaw]){
                        bestRaw =i;
                    }
                }
                return getRaw(bestRaw);
            case DEBUFFACTION:
                for(int i =0;i<getObservedEnv().getAgentRawValue().length;i++){
                    if(getObservedEnv().getAgentRawValue()[i]<getObservedEnv().getAgentRawValue()[bestRaw]){
                        bestRaw =i;
                    }
                }
                return getRaw(bestRaw);
        }
        return null;
    }


    @Override
    public void setPlayerNumber(String player) {
        super.setPlayerNumber(player);
    }

}
