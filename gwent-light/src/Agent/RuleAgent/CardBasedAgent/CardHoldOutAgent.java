package Agent.RuleAgent.CardBasedAgent;

import enums.PassOrPlaceCard;
import enums.NextAction;
import enums.Raw;
import Agent.BotAgent;

import static enums.NextAction.*;

public class CardHoldOutAgent extends BotAgent{
    private int moves;


    public CardHoldOutAgent() {
        super("CardHoldOutAgent");
    }

    @Override
    public NextAction calculateNextAction() {
        if(getObservedEnv().getGames() ==0){
            moves++;
        }
        if(getObservedEnv().getAgentHand().containsPullMoreCard()){
            return NextAction.PULLMOREACTION;
        }
        if(!getObservedEnv().getAgentHand().anyMonsterCard()&&getObservedEnv().getAgentHand().containsBuffCard()&&getObservedEnv().getGames()>0){
            return NextAction.BUFFACTION;
        }
        if(!getObservedEnv().getAgentHand().anyMonsterCard()&&getObservedEnv().getAgentHand().containsBuffCard()&&getObservedEnv().getGames()>0){
            return DEBUFFACTION;
        }
        int j =0;
        for(boolean raw : getObservedEnv().getAgentRawDebuffed()){
            if(raw ==true){
                j++;
            }
        }
        if(j>1 &&getObservedEnv().getGames()>0&&getObservedEnv().getAgentHand().containsAntiDebuffCard()){
           return ANTIDEBUFFACTION;
        }
        if(!getObservedEnv().getAgentHand().anyMonsterCard()
                &&!getObservedEnv().getAgentHand().anyBuffCard()
                &&!getObservedEnv().getAgentHand().anyDebuffCard()
                &&getObservedEnv().getAgentHand().containsEraseCard()){
            return ERASEACTION;
        }
        if(getObservedEnv().getAgentHand().anyMonsterCard()){
            return MONSTERACTION;
        }else{
            if(getObservedEnv().getAgentHand().containsBuffCard()){
                return NextAction.BUFFACTION;
            }
            if(getObservedEnv().getAgentHand().containsDeBuffCard()){
                return DEBUFFACTION;
            }
            if(getObservedEnv().getAgentHand().containsEraseCard()){
                return ERASEACTION;
            }if(getObservedEnv().getAgentHand().containsAntiDebuffCard()){
                return ANTIDEBUFFACTION;
            }
        }
        return null;
    }

    @Override
    public PassOrPlaceCard PassOrTurn() {

        if(!getObservedEnv().getAgentHand().containsPullMoreCard()&&moves<4&&getObservedEnv().getGames()==0
                ||getObservedEnv().getOpponentFieldValue()<getObservedEnv().getAgentFieldValue()&&getObservedEnv().isOpponentPass()){
            return PassOrPlaceCard.Pass;
        }else{
            return PassOrPlaceCard.PlaceCard;
        }
    }

    @Override
    public String calculateNextMonsterCard() {
        return getObservedEnv().getAgentHand().getLowestCardName();
    }

    @Override
    public Raw calculateRaw(NextAction cardType) {
        int bestRaw =0;
        switch (cardType){
            case BUFFACTION:
                for(int i =0;i<getObservedEnv().getAgentRawValue().length;i++){
                    if(getObservedEnv().getAgentRawValue()[i]>getObservedEnv().getAgentRawValue()[bestRaw]){
                        bestRaw =i;
                    }
                }
                return getRaw(bestRaw);
            case DEBUFFACTION:
                for(int i =0;i<getObservedEnv().getAgentRawValue().length;i++){
                    if(getObservedEnv().getAgentRawValue()[i]<getObservedEnv().getAgentRawValue()[bestRaw]){
                        bestRaw =i;
                    }
                }
                return getRaw(bestRaw);
        }

        return null;
    }



    @Override
    public void setPlayerNumber(String player) {
        super.setPlayerNumber(player);
    }


}
