package Agent;

import Enviroment.Controller.ConsoleController.PlayerController;
import Enviroment.Model.Card.Card;
import Enviroment.Model.Game.Game;
import Enviroment.Model.ObservedEnvironment;
import enums.CardType;
import enums.PassOrPlaceCard;
import enums.NextAction;
import enums.Raw;

public abstract class BotAgent  implements Agent{
    private PlayerController controller;
    private ObservedEnvironment observedEnvironment;
    private boolean turnFinshed;
    private String name;

    public BotAgent(String name){
        this.controller = new PlayerController();
        observedEnvironment = new ObservedEnvironment();
        this.name = name;
    }
    @Override
    public void myTurn(Game game) {
        observedEnvironment.createState(game,controller.getPlayer().getName());
        NextAction cardType = null;
        PassOrPlaceCard passOrPlaceCard = PassOrTurn();
        switch (passOrPlaceCard) {
            case Pass -> controller.passRound();
        }
        if(passOrPlaceCard != PassOrPlaceCard.Pass){
            switch (calculateNextAction()){
                case BUFFACTION -> controller.useCard(getCardByCardType(CardType.BUFFCARD),calculateRaw(NextAction.BUFFACTION));
                case DEBUFFACTION -> controller.useCard(getCardByCardType(CardType.DEBUFFCARD),calculateRaw(NextAction.BUFFACTION));
                case ERASEACTION -> controller.useCard(getCardByCardType(CardType.ERASECARD));
                case PULLMOREACTION -> controller.useCard(getCardByCardType(CardType.PULLMORECARD));
                case MONSTERACTION -> controller.useCard(calculateNextMonsterCard());
                case ANTIDEBUFFACTION -> controller.useCard(getCardByCardType(CardType.ANTIDEBUFFCARD));
            }
        }
    }
    public String getCardByCardType(CardType cardType){
        for(Card card : observedEnvironment.getAgentHand().getCards()){
            if(card.getCartType().equals(cardType)){
                return card.getName();
            }
        }
        return null;
    }
    public Raw getRaw(int rawNumber){
        switch (rawNumber){
            case 0:
                return Raw.RAW_ONE;
            case 1:
                return Raw.RAW_TWO;
            case 2:
                return Raw.RAW_THREE;
        }
        return null;
    }




    public PlayerController getController() {
        return controller;
    }

    public void setController(PlayerController controller) {
        this.controller = controller;
    }

    public ObservedEnvironment getObservedEnv() {
        return observedEnvironment;
    }

    public void setState(ObservedEnvironment observedEnvironment) {
        this.observedEnvironment = observedEnvironment;
    }

    public boolean isTurnFinshed() {
        return turnFinshed;
    }

    public void setTurnFinshed(boolean turnFinshed) {
        this.turnFinshed = turnFinshed;
    }

    public String getName(){
        return name;
    }
    public void setName(String name ){
        this.name = name;
    }

    public void setPlayerNumber(String number ){
        name = number + name;
    }


}
