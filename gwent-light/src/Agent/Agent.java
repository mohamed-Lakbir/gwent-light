package Agent;

import Enviroment.Controller.ConsoleController.PlayerController;
import Enviroment.Model.Game.Game;
import enums.PassOrPlaceCard;
import enums.NextAction;
import enums.Raw;

public interface Agent {


    public NextAction calculateNextAction();
    public PassOrPlaceCard PassOrTurn();
    public String calculateNextMonsterCard();
    public void myTurn(Game game);
    public Raw calculateRaw(NextAction action);
    public PlayerController getController();
    public String getName();
    public void setPlayerNumber(String player);

}
