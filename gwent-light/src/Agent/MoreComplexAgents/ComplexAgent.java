package Agent.MoreComplexAgents;
import Agent.*;
import Enviroment.Model.Card.Card;
import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Card.CardStacks.RawStack;
import Enviroment.Model.Card.MagicCard.Buff.AntiDebuffCard;
import Enviroment.Model.Card.MagicCard.Other.EraseCard;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import enums.*;

import java.awt.geom.Area;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Arrays;

public class ComplexAgent extends BotAgent {

    private int roundCounter;

    private int strongMonsterCards;
    private int mediumStrongMonsterCards;
    private int mediumWeakMonsterCards;
    private int weakMonsterCards;

    private int eraseCards;
    private int pullMoreCards;
    private int antiDebuffCards;
    private int buffCards;
    private int debuffCards;

    private int ownBestRawValue;
    private int rawValues[];

    private int rawOneHandValue;
    private int rawTwoHandValue;
    private int rawThreehandValue;
    private int rawValuesOverAll;

    private Raw bestAgentRawFieldAndHand;



    private int opponnentHand;
    private int opponentDeckSize;

    private int opponenntPlayedEraseCard;
    private int opponnentPlayedPullMoreCard;
    private int oponnentPlayedAntiDebuffCard;
    private int opponenntPlayedDebuffCard;
    private int opponenntPlayedBuffCard;

    private int opponentBestRawValue;
    private int opponentValueOverAll;
    private int opponentRawValue[];
    private boolean opponnentPass;

    private boolean holdOutOrFullEngage;


    private double [] oponnentMonsterCardsPossibility;
    private double [] oponentEeraseCardsPossibility;
    private double [] oponnentPullMoreCardsPossibility;
    private double [] oponnentAntiDebuffCardsPossibility;
    private double [] oponnentBuffCardsPossibility;
    private double [] oponnentDebuffCarsPossibility;


    public ComplexAgent() {
        super("ComplexAgent");
        oponentEeraseCardsPossibility = new double[2];
        oponnentPullMoreCardsPossibility= new double[2];
        oponnentAntiDebuffCardsPossibility= new double[2];
        oponnentBuffCardsPossibility= new double[2];
        oponnentDebuffCarsPossibility= new double[2];
        rawValues = new int[3];
        opponentRawValue = new int[3];
        rawOneHandValue=0;
        rawTwoHandValue=0;
        rawThreehandValue=0;
    }

    private void reset(){

    }
    @Override
    public PassOrPlaceCard PassOrTurn() {
        analysisSituation();
        String s = toString();
        return shouldIPass();
    }

    @Override
    public NextAction calculateNextAction() {
        if(whatIsMyNextAction().equals(NextAction.ANTIDEBUFFACTION)){
            return whatIsMyNextAction();
        }
        return whatIsMyNextAction();

    }

    @Override
    public String calculateNextMonsterCard() {
        String lowestCard = "";
        int lowestValue=0;
        if(holdOutOrFullEngage){
            switch (bestAgentRawFieldAndHand){
                case RAW_ONE :
                    for(Card c: getObservedEnv().getAgentHand().getCards()){
                        if(c.getCartType().equals(CardType.MONSTERCARD)){
                            if(((MonsterCard) c).getRaw().equals(Raw.RAW_ONE)){
                                if(lowestValue<=((MonsterCard) c).getValue()){
                                    lowestValue = ((MonsterCard) c).getValue();
                                    lowestCard = c.getName();
                                }
                            }
                        }
                    }
                    break;
                case RAW_TWO:
                    for(Card c: getObservedEnv().getAgentHand().getCards()){
                        if(c.getCartType().equals(CardType.MONSTERCARD)){
                            if(((MonsterCard) c).getRaw().equals(Raw.RAW_TWO)){
                                if(lowestValue<=((MonsterCard) c).getValue()){
                                    lowestValue = ((MonsterCard) c).getValue();
                                    lowestCard = c.getName();
                                }
                            }
                        }
                    }
                    break;
                case RAW_THREE:
                    for(Card c: getObservedEnv().getAgentHand().getCards()){
                        if(c.getCartType().equals(CardType.MONSTERCARD)){
                            if(((MonsterCard) c).getRaw().equals(Raw.RAW_THREE)){
                                if(lowestValue<=((MonsterCard) c).getValue()){
                                    lowestValue = ((MonsterCard) c).getValue();
                                    lowestCard = c.getName();
                                }
                            }
                        }
                    }
                    break;
            }
        }else{
            if(!getObservedEnv().getAgentHand().getLowestCard().getCartType().equals(calculatebestFieldAndHand())){
                return getObservedEnv().getAgentHand().getLowestCard().getName();
            }else {
                for(Card c: getObservedEnv().getAgentHand().getCards()){
                    if(c.getCartType().equals(CardType.MONSTERCARD)){
                        if(!((MonsterCard) c).getRaw().equals(calculatebestFieldAndHand())){
                            if(lowestValue<=((MonsterCard) c).getValue()){
                                lowestValue = ((MonsterCard) c).getValue();
                                lowestCard = c.getName();
                            }
                        }
                    }
                }
            }
        }
        if(lowestCard==""){
            return getObservedEnv().getAgentHand().getLowestCardName();
        }
        return lowestCard;
    }

    @Override
    public Raw calculateRaw(NextAction action) {
        switch (action){
            case BUFFACTION -> {
                return calculatebestFieldAndHand();
            }
            case DEBUFFACTION -> {
                return getWeakestRaw(getObservedEnv().getOpponentRawValue()[0],getObservedEnv().getOpponentRawValue()[1],getObservedEnv().getOpponentRawValue()[2]);
            }
        }
        return null;
    }

    private void analysisSituation(){
        updateOponnentInfo();
        anlysisOwnHand();
        calculatePossibilitys();
        analysisField();
        bestAgentRawFieldAndHand = calculatebestFieldAndHand();
        opponnentPass = getObservedEnv().isOpponentPass();
    }



    private PassOrPlaceCard shouldIPass(){
        if(getObservedEnv().getOpponentWins()==0&&getObservedEnv().getAgentWins()==0){
            return shouldIPass_O_O();
        }
        if(getObservedEnv().getOpponentWins()==1&&getObservedEnv().getAgentWins()==0){
            return shouldIPass_1_0();
        }
        if(getObservedEnv().getOpponentWins()==1&&getObservedEnv().getAgentWins()==1){
            return shouldIPass_1_1();
        }
        if(getObservedEnv().getOpponentWins()==0&&getObservedEnv().getAgentWins()==1){
            return shouldIPass_0_1();
        }
        return null;
    }
    //Regel ob Mann passt oder Spielt
    private PassOrPlaceCard shouldIPass_O_O(){
        if(rawValuesOverAll>opponentValueOverAll&&opponnentPass){
            return PassOrPlaceCard.Pass;
        }
        if(rawValuesOverAll<opponentValueOverAll&&rawValuesOverAll>opponentValueOverAll-6&&opponnentPass){
            return PassOrPlaceCard.PlaceCard;
        }else if(calculateRawHandValue(calculateWeakestHandAndFieldRaw())!=0){
            return PassOrPlaceCard.PlaceCard;
        }
        return  PassOrPlaceCard.Pass;
    }
    private PassOrPlaceCard shouldIPass_1_0(){
        if(rawValuesOverAll>opponentValueOverAll&&opponnentPass){
            return PassOrPlaceCard.Pass;
        }else{
            return PassOrPlaceCard.PlaceCard;
        }
/*        if(rawValuesOverAll<opponentValueOverAll){
            return PassOrPlaceCard.PlaceCard;
        }
        return null;
 */   }
    private PassOrPlaceCard shouldIPass_1_1(){
        return PassOrPlaceCard.PlaceCard;
    }
    private PassOrPlaceCard shouldIPass_0_1(){
        if(rawValuesOverAll>opponentValueOverAll&&opponnentPass){
            return PassOrPlaceCard.PlaceCard;
        }
        if(rawValuesOverAll<opponentValueOverAll&&rawValuesOverAll>opponentValueOverAll-6&&opponnentPass){
            return PassOrPlaceCard.PlaceCard;
        }else if(calculateRawHandValue(calculateWeakestHandAndFieldRaw())!=0){
            return PassOrPlaceCard.PlaceCard;
        }
        return PassOrPlaceCard.Pass;
    }
    private NextAction whatIsMyNextAction(){
        if(getObservedEnv().getOpponentWins()==0&&getObservedEnv().getAgentWins()==0){
            return whatIsMyNextAction_0_0();
        }
        if(getObservedEnv().getOpponentWins()==1&&getObservedEnv().getAgentWins()==0){
            return whatIsMyNextAction_1_0();
        }
        if(getObservedEnv().getOpponentWins()==1&&getObservedEnv().getAgentWins()==1){
            return whatIsMyNextAction_1_1();
        }
        if(getObservedEnv().getOpponentWins()==0&&getObservedEnv().getAgentWins()==1){
            return whatIsMyNextAction_0_1();
        }
        return null;
    }
    //NextAction
    private NextAction whatIsMyNextAction_0_0(){
        if(getObservedEnv().getAgentHand().containsPullMoreCard()){
            return NextAction.PULLMOREACTION;
        }
        if(calculateRawHandValue(calculateWeakestHandAndFieldRaw())!=0){
            return NextAction.MONSTERACTION;
        }
        if(getObservedEnv().getAgentHand().containsEraseCard()){
            return NextAction.ERASEACTION;
        }
        if(getObservedEnv().getAgentHand().containsDeBuffCard()){
            return NextAction.DEBUFFACTION;
        }
        if(getObservedEnv().getAgentHand().containsBuffCard()){
            return NextAction.BUFFACTION;
        }
        return wrongActions();
    }
    private NextAction whatIsMyNextAction_1_0(){
        if(getObservedEnv().getAgentHand().containsPullMoreCard()){
            return NextAction.PULLMOREACTION;
        }
        if(calculateRawHandValue(calculateWeakestHandAndFieldRaw()) !=0 && calculateRawHandValue(calculateMiddleHandAndFieldRaw())!=0){
            return NextAction.MONSTERACTION;
        }
        if(this.eraseCardWorthable()&&getObservedEnv().getAgentHand().containsEraseCard()){
            return NextAction.ERASEACTION;
        }
        if(this.debuffedWorthabel()&&getObservedEnv().getAgentHand().containsDeBuffCard()){
            return NextAction.DEBUFFACTION;
        }
        if(this.buffWorthabel()&&getObservedEnv().getAgentHand().containsBuffCard()){
            return NextAction.BUFFACTION;
        }
        if(antiDebuffCardWorthabel()&&getObservedEnv().getAgentHand().containsAntiDebuffCard()){
            return NextAction.ANTIDEBUFFACTION;
        }
        return wrongActions();
    }
    private NextAction whatIsMyNextAction_1_1(){
        if(buffWorthabel()&&getObservedEnv().getAgentHand().containsBuffCard()){
            holdOutOrFullEngage = true;
            return NextAction.BUFFACTION;
        }
        if(debuffedWorthabel()&&getObservedEnv().getAgentHand().containsDeBuffCard()){
            return NextAction.DEBUFFACTION;
        }
        if(eraseCardWorthable()&&getObservedEnv().getAgentHand().containsEraseCard()){
            return NextAction.ERASEACTION;
        }
        if(antiDebuffCardWorthabel()&&getObservedEnv().getAgentHand().containsAntiDebuffCard()){
            return NextAction.ANTIDEBUFFACTION;
        }
        if(getObservedEnv().getAgentHand().containsMonsterCard()){
            return NextAction.MONSTERACTION;
        }
        return wrongActions();
    }
    private NextAction whatIsMyNextAction_0_1(){
        if(getObservedEnv().getAgentHand().containsMonsterCard()){
            return NextAction.MONSTERACTION;
        }
        if(buffWorthabel()&&getObservedEnv().getAgentHand().containsBuffCard()){
            holdOutOrFullEngage = true;
            return NextAction.BUFFACTION;
        }
        if(debuffedWorthabel()&&getObservedEnv().getAgentHand().containsDeBuffCard()){
            return NextAction.DEBUFFACTION;
        }
        if(eraseCardWorthable()&&getObservedEnv().getAgentHand().containsEraseCard()){
            return NextAction.ERASEACTION;
        }
        if(antiDebuffCardWorthabel()&&getObservedEnv().getAgentHand().containsAntiDebuffCard()){
            return NextAction.ANTIDEBUFFACTION;
        }
        if(getObservedEnv().getAgentHand().containsMonsterCard()){
            return NextAction.MONSTERACTION;
        }
        return wrongActions();
    }

    public NextAction wrongActions(){
      //  System.err.println("Something went wrong, using not decided Cards");
        CardStack c = getObservedEnv().getAgentHand();
        if(getObservedEnv().getAgentHand().containsMonsterCard()){
            return NextAction.MONSTERACTION;
        }
        if(getObservedEnv().getAgentHand().containsPullMoreCard()){
            return NextAction.PULLMOREACTION;
        }
        if(getObservedEnv().getAgentHand().containsEraseCard()){
            return NextAction.ERASEACTION;
        }
        if(getObservedEnv().getAgentHand().containsDeBuffCard()){
            return NextAction.DEBUFFACTION;
        }
        if(getObservedEnv().getAgentHand().containsBuffCard()){
            return NextAction.BUFFACTION;
        }
        if(getObservedEnv().getAgentHand().containsAntiDebuffCard()){
            return NextAction.ANTIDEBUFFACTION;
        }

        return null;
    }
    public void analysisField(){
        rawValues[0] = getObservedEnv().getTable().getOwnField(getName()).getFirstRawStack().getValue();
        rawValues[1] = getObservedEnv().getTable().getOwnField(getName()).getSecondRawStack().getValue();
        rawValues[2] = getObservedEnv().getTable().getOwnField(getName()).getThirdRawStack().getValue();

        opponentRawValue[0]= getObservedEnv().getTable().getOpponentField(getName()).getFirstRawStack().getValue();
        opponentRawValue[1]=getObservedEnv().getTable().getOpponentField(getName()).getSecondRawStack().getValue();
        opponentRawValue[2]=getObservedEnv().getTable().getOpponentField(getName()).getThirdRawStack().getValue();

        opponentBestRawValue = calculateBestRaw (opponentRawValue);
        ownBestRawValue = calculateBestRaw (rawValues);


        ownBestRawValue = getObservedEnv().getAgentFieldValue();
    }
    boolean compareTwoRawsValues(int a,int b){
        return a>b;
    }
    private int calculateBestRaw(int rawValues[]){
        int best = 0;
        for(int i=0;i<rawValues.length;i++){
            if(rawValues[i]>best){
                best = rawValues[i];
            }
        }
        return best;
    }
    private Raw calculatebestFieldAndHand(){
        int rawOneValue =0;
        int rawTwoValue =0;
        int rawThreeValue=0;

        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getFirstRawStack().getCards()){
            rawOneValue = rawOneValue + ((MonsterCard)(c)).getValue();
        }
        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getSecondRawStack().getCards()){
            rawTwoValue = rawTwoValue + ((MonsterCard)(c)).getValue();
        }
        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getThirdRawStack().getCards()){
            rawThreeValue = rawThreeValue + ((MonsterCard)(c)).getValue();
        }
        rawOneValue = rawOneValue + calculateRawHandValue(Raw.RAW_ONE);
        rawTwoValue = rawTwoValue + calculateRawHandValue(Raw.RAW_TWO);
        rawThreeValue = rawThreeValue + calculateRawHandValue(Raw.RAW_THREE);
        return getBestRaw(rawOneValue,rawTwoValue,rawThreeValue);
    }
    public Raw calculateWeakestHandAndFieldRaw(){
        int rawOneValue =0;
        int rawTwoValue =0;
        int rawThreeValue=0;
        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getFirstRawStack().getCards()){
            rawOneValue = rawOneValue + ((MonsterCard)(c)).getValue();
        }
        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getSecondRawStack().getCards()){
            rawTwoValue = rawTwoValue + ((MonsterCard)(c)).getValue();
        }
        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getThirdRawStack().getCards()){
            rawThreeValue = rawThreeValue + ((MonsterCard)(c)).getValue();
        }
        rawOneValue = rawOneValue + calculateRawHandValue(Raw.RAW_ONE);
        rawTwoValue = rawTwoValue + calculateRawHandValue(Raw.RAW_TWO);
        rawThreeValue = rawThreeValue + calculateRawHandValue(Raw.RAW_THREE);

        return getBestRaw(rawOneValue,rawTwoValue,rawThreeValue);
    }

    public Raw calculateMiddleHandAndFieldRaw(){
        int rawOneValue =0;
        int rawTwoValue =0;
        int rawThreeValue=0;
        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getFirstRawStack().getCards()){
            rawOneValue = rawOneValue + ((MonsterCard)(c)).getValue();
        }
        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getSecondRawStack().getCards()){
            rawTwoValue = rawTwoValue + ((MonsterCard)(c)).getValue();
        }
        for(Card c : getObservedEnv().getTable().getOwnField(getName()).getThirdRawStack().getCards()){
            rawThreeValue = rawThreeValue + ((MonsterCard)(c)).getValue();
        }
        rawOneValue = rawOneValue + calculateRawHandValue(Raw.RAW_ONE);
        rawTwoValue = rawTwoValue + calculateRawHandValue(Raw.RAW_TWO);
        rawThreeValue = rawThreeValue + calculateRawHandValue(Raw.RAW_THREE);

        if(calculateMiddleRaw(rawOneValue,rawTwoValue,rawThreeValue)==null){
            System.err.println("null");
        }
        return calculateMiddleRaw(rawOneValue,rawTwoValue,rawThreeValue);
    }


    private Raw getWeakestRaw(int rawOne,int rawTwo,int rawThree){
        if(rawOne<=rawTwo&&rawOne<=rawThree){
            return Raw.RAW_ONE;
        }else if(rawTwo<=rawOne&&rawTwo<=rawThree){
            return Raw.RAW_TWO;
        }else{
            return Raw.RAW_THREE;
        }
    }
    private Raw getBestRaw(int rawOne,int rawTwo,int rawThree){
        if(rawOne>=rawTwo&&rawOne>=rawThree){
            return Raw.RAW_ONE;
        }else if(rawTwo>=rawOne&&rawTwo>=rawThree){
            return Raw.RAW_TWO;
        }else{
            return Raw.RAW_THREE;
        }
    }
    public int calculateRawHandValue(Raw raw){
        int sum=0;
        switch (raw) {
            case RAW_ONE :{
                for(Card c : getObservedEnv().getTable().getOwnField(getName()).getHand().getCards()){
                    if(c.getCartType().equals(CardType.MONSTERCARD)){
                        if(((MonsterCard)(c)).getRaw().equals(Raw.RAW_ONE))
                        sum = sum+ ((MonsterCard)(c)).getValue();
                    }
                }
                break;
            }
            case RAW_TWO :{
                for(Card c : getObservedEnv().getTable().getOwnField(getName()).getHand().getCards()){
                    if(c.getCartType().equals(CardType.MONSTERCARD)){
                        if(((MonsterCard)(c)).getRaw().equals(Raw.RAW_TWO))
                            sum = sum+ ((MonsterCard)(c)).getValue();
                    }
                }
                break;
            }
            case RAW_THREE: {
                for(Card c : getObservedEnv().getTable().getOwnField(getName()).getHand().getCards()){
                    if(c.getCartType().equals(CardType.MONSTERCARD)){
                        if(((MonsterCard)(c)).getRaw().equals(Raw.RAW_THREE))
                            sum = sum+ ((MonsterCard)(c)).getValue();
                    }
                }
                break;
            }
        }
        return sum;
    }
    private void anlysisOwnHand(){
        CardStack hand = getObservedEnv().getAgentHand();
        strongMonsterCards =0;
        mediumStrongMonsterCards =0;
        mediumWeakMonsterCards = 0;
        strongMonsterCards =0;
        weakMonsterCards =0;
        eraseCards =0;
        debuffCards=0;
        pullMoreCards =0;
        antiDebuffCards=0;

        for(Card c : hand.getCards()){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                if(((MonsterCard)(c)).getValue()>14){
                    strongMonsterCards++;
                }
                if(c.getCartType().equals(CardType.MONSTERCARD)){
                    if(((MonsterCard)(c)).getValue()<15&&((MonsterCard)(c)).getValue()>9){
                        mediumStrongMonsterCards++;
                    }
                }
                if(c.getCartType().equals(CardType.MONSTERCARD)){
                    if(((MonsterCard)(c)).getValue()<10&&((MonsterCard)(c)).getValue()>4){
                        mediumWeakMonsterCards++;
                    }
                }
                if(((MonsterCard)(c)).getValue()<5){
                    weakMonsterCards++;
                }
                if(((MonsterCard)(c)).getRaw().equals(Raw.RAW_ONE)){
                    rawOneHandValue = rawOneHandValue + ((MonsterCard)(c)).getValue();
                }
                if(((MonsterCard)(c)).getRaw().equals(Raw.RAW_TWO)){
                    rawTwoHandValue = rawTwoHandValue + ((MonsterCard)(c)).getValue();
                }
                if(((MonsterCard)(c)).getRaw().equals(Raw.RAW_THREE)){
                    rawThreehandValue = rawThreehandValue + ((MonsterCard)(c)).getValue();
                }
            }
            if(c.getCartType().equals(CardType.ERASECARD)){
                eraseCards++;
            }
            if(c.getCartType().equals(CardType.ANTIDEBUFFCARD)){
                antiDebuffCards++;
            }
            if(c.getCartType().equals(CardType.DEBUFFCARD)){
                debuffCards++;            }
            if(c.getCartType().equals(CardType.BUFFCARD)){
                buffCards++;
            }
            if(c.getCartType().equals(CardType.PULLMORECARD)){
                pullMoreCards++;
            }
        }
    }
    public void calculateMonsterCardPossibility(){
        oponnentMonsterCardsPossibility = new double[opponnentHand];
        for(int i=0;i < opponnentHand-1;i++){
            oponnentMonsterCardsPossibility [i] = calcualteHypergeometricDistribution(new BigDecimal(opponentDeckSize),new BigDecimal(opponnentHand),new BigDecimal(opponnentHand),new BigDecimal(i)).doubleValue();
        }
    }
    private void calculatePossibilitys(){
        oponentEeraseCardsPossibility = calculatePossibilityForOne(oponentEeraseCardsPossibility,opponenntPlayedEraseCard);
        calculatePossibilityForOne(oponnentBuffCardsPossibility,opponenntPlayedBuffCard);
        calculatePossibilityForOne(oponnentDebuffCarsPossibility,opponenntPlayedDebuffCard);
        calculatePossibilityForOne(oponnentPullMoreCardsPossibility,opponnentPlayedPullMoreCard);
        calculatePossibilityForOne(oponnentAntiDebuffCardsPossibility,oponnentPlayedAntiDebuffCard);
        calculateMonsterCardPossibility();
        /*
        oponentEeraseCardsPossibility = new double[2];
        oponnentPullMoreCardsPossibility= new double[2];
        oponnentAntiDebuffCardsPossibility= new double[2];
        oponnentDebuffCarsPossibility= new double[2];
        */
     }

     private Raw calculateMiddleRaw(int rawOne,int rawTwo,int rawThree){
        Raw weak = getWeakestRaw(rawOne,rawTwo,rawThree);
        Raw best = getBestRaw(rawOne,rawTwo,rawThree);

        if(weak.equals(Raw.RAW_ONE)&&best.equals(Raw.RAW_TWO)||best.equals(Raw.RAW_ONE)&&weak.equals(Raw.RAW_TWO)){
            return Raw.RAW_THREE;
        }
        if(weak.equals(Raw.RAW_TWO)&&best.equals(Raw.RAW_THREE)||best.equals(Raw.RAW_TWO)&&weak.equals(Raw.RAW_THREE)){
             return Raw.RAW_ONE;
        }
        if(weak.equals(Raw.RAW_ONE)&&best.equals(Raw.RAW_THREE)||best.equals(Raw.RAW_ONE)&&weak.equals(Raw.RAW_THREE)){
             return Raw.RAW_TWO;
        }
        return null;
     }

     private boolean eraseCardWorthable(){
        int heighestCard =0;
        for(Card c: getObservedEnv().getTable().getOpponentField(getName()).getFirstRawStack().getCards()){
            if(((MonsterCard)(c)).getValue()>heighestCard){
                heighestCard = ((MonsterCard)(c)).getValue();
            }
        }
        for(Card c: getObservedEnv().getTable().getOpponentField(getName()).getFirstRawStack().getCards()){
            if(((MonsterCard)(c)).getValue()>heighestCard){
                heighestCard = ((MonsterCard)(c)).getValue();
            }
        }
        for(Card c: getObservedEnv().getTable().getOpponentField(getName()).getFirstRawStack().getCards()){
            if(((MonsterCard)(c)).getValue()>heighestCard){
                heighestCard = ((MonsterCard)(c)).getValue();
            }
        }
        int doubles=0;
        for(Card c: getObservedEnv().getTable().getOpponentField(getName()).getFirstRawStack().getCards()){
             if(((MonsterCard)(c)).getValue()==heighestCard){
                 doubles++;
             }
         }
         for(Card c: getObservedEnv().getTable().getOpponentField(getName()).getFirstRawStack().getCards()){
             if(((MonsterCard)(c)).getValue()==heighestCard){
                 doubles++;
             }
         }
         for(Card c: getObservedEnv().getTable().getOpponentField(getName()).getFirstRawStack().getCards()){
             if(((MonsterCard)(c)).getValue()==heighestCard){
                 doubles++;
             }
         }
         if(heighestCard==0){
             return false;
         }else if(doubles>1){
             return true;
         }
         return false;
     }
    private boolean antiDebuffCardWorthabel(){
        for(int i=0;i<getObservedEnv().getAgentRawDebuffed().length;i++){
            if(getObservedEnv().getAgentRawDebuffed()[i]){
                int valueWithoutDebuff =0;
                switch (i){
                    case 0:
                        for(Card c: getObservedEnv().getTable().getOwnField(getName()).getFirstRawStack().getCards()){
                            valueWithoutDebuff = valueWithoutDebuff +((MonsterCard) c).getValue();
                        }
                        if(valueWithoutDebuff>20){
                            return true;
                        }
                        break;
                    case 1:
                        for(Card c: getObservedEnv().getTable().getOwnField(getName()).getSecondRawStack().getCards()){
                            valueWithoutDebuff = valueWithoutDebuff +((MonsterCard) c).getValue();
                        }
                        if(valueWithoutDebuff>20){
                            return true;
                        }
                        break;
                    case 2:
                        for(Card c: getObservedEnv().getTable().getOwnField(getName()).getThirdRawStack().getCards()){
                            valueWithoutDebuff = valueWithoutDebuff +((MonsterCard) c).getValue();
                        }
                        if(valueWithoutDebuff>20){
                            return true;
                        }
                        break;
                }
                return true;
            }
        }
        return false;
    }
    private boolean debuffedWorthabel(){
        if(getObservedEnv().isOpponentPass()){
            if(getObservedEnv().getOpponentFieldValue()-getObservedEnv().getTable().getOpponentField(getName()).getFirstRawStack().getCards().size()<getObservedEnv().getAgentFieldValue()){
                return true;
            }
            if(getObservedEnv().getOpponentFieldValue()-getObservedEnv().getTable().getOpponentField(getName()).getSecondRawStack().getCards().size()<getObservedEnv().getAgentFieldValue()){
                return true;
            }
            if(getObservedEnv().getOpponentFieldValue()-getObservedEnv().getTable().getOpponentField(getName()).getThirdRawStack().getCards().size()<getObservedEnv().getAgentFieldValue()){
                return true;
            }
        }
        for(int i=0;i< getObservedEnv().getOpponentRawBuffed().length;i++){
            if(getObservedEnv().getOpponentRawBuffed()[i]){
                return true;
            }
        }
        if(getObservedEnv().getTable().getNumberOfOponnentUsedAntiDebuffCard(getName())>0&&(opponentBestRawValue<20)&&getObservedEnv().getAgentWins()+getObservedEnv().getOpponentWins()>0){
            return true;
        }
        return false;
    }

    private boolean buffWorthabel(){
        if(getObservedEnv().isOpponentPass()){
           if(ownBestRawValue*2>opponentValueOverAll){
               return true;
           }
        }
        if(buffCards>2){
            return true;
        }
        return false;
    }


     private double [] calculatePossibilityForOne(double possibility[],int played){
        if(played ==0){
            possibility[0] = calcualteHypergeometricDistribution(new BigDecimal(opponentDeckSize),new BigDecimal(opponnentHand),new BigDecimal(2),new BigDecimal(1)).doubleValue();
            possibility[1] = calcualteHypergeometricDistribution(new BigDecimal(opponentDeckSize),new BigDecimal(opponnentHand),new BigDecimal(2),new BigDecimal(2)).doubleValue();
        }
        if(played ==1){
            possibility[0] = calcualteHypergeometricDistribution(new BigDecimal(opponentDeckSize),new BigDecimal(opponnentHand),new BigDecimal(1),new BigDecimal(1)).doubleValue();
            possibility[1] = 0;
        }
        if(played ==2){
            possibility[0] = 0;
            possibility[1] = 0;
        }
        return possibility;
    }
    private void updateOponnentInfo(){
        opponnentHand = getObservedEnv().getOpponentHand();
        opponentDeckSize = getObservedEnv().getOpponentDeck();
        opponenntPlayedEraseCard = getObservedEnv().getTable().getNumberOfOponnentUsedEraseCard(getName());
        opponnentPlayedPullMoreCard = getObservedEnv().getTable().getNumberOfOponnentUsedPullMoreCard(getName());
        oponnentPlayedAntiDebuffCard = getObservedEnv().getTable().getNumberOfOponnentUsedDebuffCard(getName());
        opponenntPlayedDebuffCard = getObservedEnv().getTable().getNumberOfOponnentUsedDebuffCard(getName());
        opponenntPlayedBuffCard = getObservedEnv().getTable().getNumberOfOponnentUsedBuffCard(getName());
        opponentValueOverAll = getObservedEnv().getOpponentFieldValue();
        rawValuesOverAll = getObservedEnv().getAgentFieldValue();
    }
    private BigDecimal calcualteHypergeometricDistribution(BigDecimal deckSize,BigDecimal handSize,BigDecimal cardFequenzy,BigDecimal frequenzy){
            BigDecimal nenner = calculateNOverK(deckSize,handSize);
            BigDecimal zähler = calculateNOverK(cardFequenzy,frequenzy).multiply(calculateNOverK(deckSize.subtract(cardFequenzy),handSize.subtract(frequenzy)));


        return zähler.divide(nenner,3,RoundingMode.HALF_UP);
    }
    private  BigDecimal calculateNOverK(BigDecimal n, BigDecimal k){
        BigDecimal nFak = calculateFak(n);
        BigDecimal kFak = calculateFak(k);
        BigDecimal nMinusKFak = calculateFak(n.subtract(k));
        return nFak.divide((kFak.multiply(nMinusKFak)),5, RoundingMode.HALF_UP);
    }
    private BigDecimal calculateFak(BigDecimal f){
        BigDecimal start = new BigDecimal(1);
        for(int i=1;f.compareTo(new BigDecimal(i))==1;i++){
            start =start.add(start.multiply(new BigDecimal(i)));
        }
        return start;
    }
    @Override
    public String toString() {
        return "ComplexAgent{" +
                "strongMonsterCards=" + strongMonsterCards + "\n"+
                ", mediumStrongMonsterCards=" + mediumStrongMonsterCards +"\n"+
                ", mediumWeakMonsterCards=" + mediumWeakMonsterCards +"\n"+
                ", weakMonsterCards=" + weakMonsterCards +"\n"+
                ", eraseCards=" + eraseCards +"\n"+
                ", pullMoreCards=" + pullMoreCards +"\n"+
                ", antiDebuffCards=" + antiDebuffCards +"\n"+
                ", buffCards=" + buffCards +"\n"+
                ", debuffCards=" + debuffCards +"\n"+
                ", opponnentHand=" + opponnentHand +"\n"+
                ", opponentDeckSize=" + opponentDeckSize +"\n"+
                ", oponnentMonsterCardsPossibility=" + Arrays.toString(oponnentMonsterCardsPossibility) +"\n"+
                ", oponentEeraseCardsPossibility=" + Arrays.toString(oponentEeraseCardsPossibility) +"\n"+
                ", oponnentPullMoreCardsPossibility=" + Arrays.toString(oponnentPullMoreCardsPossibility) +"\n"+
                ", oponnentAntiDebuffCardsPossibility=" + Arrays.toString(oponnentAntiDebuffCardsPossibility) +"\n"+
                ", oponnentBuffCardsPossibility=" + Arrays.toString(oponnentBuffCardsPossibility) +"\n"+
                ", oponnentDebuffCarsPossibility=" + Arrays.toString(oponnentDebuffCarsPossibility) +"\n"+
                ", opponenntPlayedEraseCard=" + opponenntPlayedEraseCard +"\n"+
                ", opponnentPlayedPullMoreCard=" + opponnentPlayedPullMoreCard +"\n"+
                ", oponnentPlayedAntiDebuffCard=" + oponnentPlayedAntiDebuffCard +"\n"+
                ", opponenntPlayedDebuffCard=" + opponenntPlayedDebuffCard +"\n"+
                ", opponenntPlayedBuffCard=" + opponenntPlayedBuffCard +"\n"+
                '}';
    }
    public static void main(String[]args){
       // System.out.println(new ComplexAgent().calcualteHypergeometricDistribution(new BigDecimal(30),new BigDecimal(10),new BigDecimal(20),new BigDecimal(8)));
    }
}
