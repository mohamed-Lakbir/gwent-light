package Agent;

import Enviroment.Controller.ConsoleController.PlayerController;
import Enviroment.Model.Game.Game;
import Enviroment.Model.ObservedEnvironment;
import enums.PassOrPlaceCard;
import enums.NextAction;
import enums.Raw;

import java.util.Scanner;

public class HumanAgent implements Agent {

    private Scanner scanner;
    private PlayerController playerController;
    private ObservedEnvironment observedEnvironment;
    private String name;



    public HumanAgent(){
         scanner = new Scanner(System.in);
         this.playerController = new PlayerController();
        observedEnvironment = new ObservedEnvironment();
        name = "Human Player";

    }


    @Override
    public NextAction calculateNextAction() {
        return null;
    }

    @Override
    public PassOrPlaceCard PassOrTurn() {
        return null;
    }

    @Override
    public String calculateNextMonsterCard() {
        return null;
    }

    public void myTurn(Game game) {
        boolean action= false;
        String input;
        while (!action) {
            System.out.println("Which is your next action.");
            System.out.println("usecard for using a card,");
            System.out.println("pass for passing the game");
            input = scanner.next();
            if (input.equals("usecard")) {
                    System.out.println("Which card do you want to use");
                    input = scanner.next();
                    if(playerController.checkForCard(input)){
                        if(input.toLowerCase().equals("buff")||input.toLowerCase().equals("debuff")){
                            String cardname = input;
                            while (!input.equals(1)||!input.equals(2)||!input.equals(3)){
                                System.out.println("Which do you want to place this card, 1, 2, or 3?");
                                input = scanner.next();
                                if(input.equals("1")){
                                    playerController.useCard(cardname, Raw.RAW_ONE);
                                    action = true;
                                    break;
                                }
                                if(input.equals("2")){
                                    playerController.useCard(cardname, Raw.RAW_TWO);
                                    action = true;
                                    break;
                                }
                                if(input.equals("3")){
                                    playerController.useCard(cardname, Raw.RAW_THREE);
                                    action = true;
                                    break;
                                }
                            }
                        }else {
                        playerController.useCard(input);
                        action = true;
                        }
                    }else{
                        System.err.println("This card doesnt exist in this Hand");
                    }
            }else if(input.equals("pass")){
                playerController.passRound();
                action = true;
            }else{
                System.err.println("This command doesnt exist!");
            }
        }
    }



    @Override
    public Raw calculateRaw(NextAction cardType) {
        return null;
    }

    @Override
    public PlayerController getController() {
        return playerController;
    }

    @Override
    public String getName() {
        return "Human Player";
    }

    @Override
    public void setPlayerNumber(String player) {

    }
    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public PlayerController getPlayerController() {
        return playerController;
    }

    public void setPlayerController(PlayerController playerController) {
        this.playerController = playerController;
    }

    public ObservedEnvironment getObservedEnvironment() {
        return observedEnvironment;
    }

    public void setObservedEnvironment(ObservedEnvironment observedEnvironment) {
        this.observedEnvironment = observedEnvironment;
    }

    public void setName(String name) {
        this.name = name;
    }
}
