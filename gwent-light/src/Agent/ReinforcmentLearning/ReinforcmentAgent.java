package Agent.ReinforcmentLearning;

import Agent.Agent;

public interface ReinforcmentAgent extends Agent {
    public void play();
    public void train();
}
