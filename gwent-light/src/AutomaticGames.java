import Agent.*;
import Enviroment.Model.Game.MainSystem;

public class AutomaticGames {
    private Agent agents[];
    private MainSystem mainSystem;

    public AutomaticGames(Agent agents[]){
        this.agents = agents;
    }

    public void startGameSequences(Agent test,int numberOfGames){
        String testName = test.getName();
        for(Agent agent : agents){
            mainSystem = new MainSystem(agent,test);
            mainSystem.games(numberOfGames);
            BotAgent botagent = (BotAgent) agent;
            botagent.setName(testName);
        }
    }
}
