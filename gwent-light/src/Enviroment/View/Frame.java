package Enviroment.View;

import Agent.Agent;
import Enviroment.Controller.ConsoleController.PlayerController;
import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Field.Field;
import Enviroment.Model.Field.Table;
import Enviroment.Model.Game.Game;
import Enviroment.Model.Game.GameControllerObserver;
import Enviroment.Model.ObservedEnvironment;
import enums.NextAction;
import enums.PassOrPlaceCard;
import enums.Raw;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Frame  extends JFrame{

    private ObservedEnvironment env;
    private PlayerController controller;
    private String name;
    private CardStack ownHand;
    private int opponentHand;
    private Table table;

    public Frame(ObservedEnvironment env, PlayerController controller){
        super("Gwent-Light");
        this.setLayout(new BorderLayout());
        this.setBackground(Color.GREEN);
        this.env=env;
        this.controller = controller;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1280,1080);
        setResizable(false);
        setVisible(true);
    }
    public void buttonMethod(){


    }
    public void updateFrame(){
        name = controller.getPlayer().getName();
        ownHand = env.getAgentHand();
        opponentHand = env.getOpponentHand();
        table = env.getTable();
    }
}
