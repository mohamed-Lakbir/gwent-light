package Enviroment.Model.Ability;

import Enviroment.Model.Field.Field;

public class PullMore implements Ability{
    @Override
    public void useAbility(Field field) {
        field.pullCards(3);

    }
}
