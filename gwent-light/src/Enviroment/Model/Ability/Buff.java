package Enviroment.Model.Ability;

import Enviroment.Model.Field.Field;
import enums.Raw;

public class Buff implements Ability{

    private Raw buffRaw;



    public void setRaw(Raw rawStack){
        buffRaw = rawStack;
    }
    @Override
    public void useAbility(Field field) {
        switch (buffRaw){
            case RAW_ONE:
                field.getFirstRawStack().setBuffed(true);
                break;
            case RAW_TWO:
                field.getSecondRawStack().setBuffed(true);
                break;
            case RAW_THREE:
                field.getThirdRawStack().setBuffed(true);
                break;
        }
    }

}
