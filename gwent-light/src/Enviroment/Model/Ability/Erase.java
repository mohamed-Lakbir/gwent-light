package Enviroment.Model.Ability;

import Enviroment.Model.Card.Card;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import Enviroment.Model.Field.Field;

import java.util.LinkedList;

public class Erase implements Ability{

    @Override
    public void useAbility(Field field) {
        LinkedList<Card> bestCards = new LinkedList<>();
        int bestPoints=0;
        for(Card c : field.getFirstRawStack().getCards()){
            if(((MonsterCard) c).getValue()>bestPoints){
                bestCards.clear();
                bestCards.add(c);
                bestPoints = ((MonsterCard) c).getValue();
            } else if(((MonsterCard)c).getValue()==bestPoints){
                bestCards.add(c);
            }
        }
        for(Card c : field.getSecondRawStack().getCards()){
            if(((MonsterCard) c).getValue()>bestPoints){
                bestCards.clear();
                bestCards.add(c);
                bestPoints = ((MonsterCard) c).getValue();
            } else if(((MonsterCard)c).getValue()==bestPoints){
                bestCards.add(c);
            }
        }
        for(Card c : field.getThirdRawStack().getCards()){
            if(((MonsterCard) c).getValue()>bestPoints){
                bestCards.clear();
                bestCards.add(c);
                bestPoints = ((MonsterCard) c).getValue();

            } else if(((MonsterCard)c).getValue()==bestPoints){
                bestCards.add(c);
            }
        }
        for(Card bestCard : bestCards){
            field.getFirstRawStack().getCards().removeIf(c -> c.getName().equals(bestCard.getName()));
            field.getSecondRawStack().getCards().removeIf(c -> c.getName().equals(bestCard.getName()));
            field.getThirdRawStack().getCards().removeIf(c -> c.getName().equals(bestCard.getName()));
        }
    }
}
