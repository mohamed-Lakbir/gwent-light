package Enviroment.Model.Ability;

import Enviroment.Model.Field.Field;

public interface Ability {
    public void useAbility(Field field);

}
