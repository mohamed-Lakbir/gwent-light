package Enviroment.Model.Ability;

import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Field.Field;
import enums.Raw;

public class AntiDebuffAbillity implements Ability {

    @Override
    public void useAbility(Field field) {
        if (field.checkIfRawIsDebuffes(Raw.RAW_ONE)) {
            field.getFirstRawStack().setDeBuffed(false);
        }
        if (field.checkIfRawIsDebuffes(Raw.RAW_TWO)) {
            field.getSecondRawStack().setDeBuffed(false);
        }
        if (field.checkIfRawIsDebuffes(Raw.RAW_THREE)) {
            field.getThirdRawStack().setDeBuffed(false);
        }
    }
}
