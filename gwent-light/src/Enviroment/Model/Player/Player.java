package Enviroment.Model.Player;

import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Field.Field;
import Enviroment.Model.Field.Table;
import enums.Raw;

public class Player {

    private CardStack deck;
    private Table table;
    private String name;
    private boolean pass;
    private boolean turn;
    private int wins;
    private int draws;
    private int looses;

    public Player(String name){
        this.name = name;
    }

    public void setNextTable(Table table){
        this.table = table;
    }

    public int getValue(){
        return table.getValue(name);
    }

    public void setUpGame(Table table){
        CardStack deckCopy = CardStack.deepCopy(deck);
        deckCopy.mixCards();
        this.table = table;
        table.setUpTable(deckCopy,this.name);
    }
    public void placeTableOnField(){

        table.setUpTable(this.deck,this.name);
    }

    public void mixDeck(){

        this.deck.mixCards();
    }




    public void startTurn(){

        this.turn = true;
    }
    public void win(){

        wins++;
    }
    public void pullCard(int number){

       /* for(int i=0;i<number;i++){
            if(deck.getCards().size()==0){
                System.err.println("Keine Karten mehr im deck");
            }else{
                field.getHand().add(deck.dropLast());
            }
        }
        */
        table.pullCard(name,number);

    }
    public void draws(){
        draws++;
    }

    public void passRound(){
        this.pass = true;
    }


    public void placeCard(String cardName){
        this.table.playerUseCard(this.name,cardName);
    }


    public void placeCard(String cardName, Raw raw){
        this.table.playerUseCard(this.name,cardName,raw);
    }

    public void reset(){
        this.wins=0;
        this.draws =0;
        this.pass = false;
    }



    public CardStack getDeck() {
        return deck;
    }

    public void setDeck(CardStack deck) {
        this.deck = deck;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public boolean isPass() {
        return pass;
    }

    public void setPass(boolean pass) {
        this.pass = pass;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }


    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public int getLooses() {
        return looses;
    }

    public void setLooses(int looses) {
        this.looses = looses;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}
