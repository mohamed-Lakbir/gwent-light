package Enviroment.Model.Card.MagicCard.Buff;

import Enviroment.Model.Ability.Buff;
import Enviroment.Model.Card.MagicCard.MagicCard;
import Enviroment.Model.Field.Field;
import enums.CardType;
import enums.Raw;

public class BuffCard implements MagicCard {
    private Buff buff;
    final private CardType cardType = CardType.BUFFCARD;
    public BuffCard(){
        buff = new Buff();
    }

    public void setUpRaw(Raw raw){
        buff.setRaw(raw);
    }

    @Override
    public String getName() {
        return "Buff";
    }

    @Override
    public CardType getCartType() {
        return this.cardType;
    }

    @Override
    public void activateAbility(Field field) {
        buff.useAbility(field);
    }
}
