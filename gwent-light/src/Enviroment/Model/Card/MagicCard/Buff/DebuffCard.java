package Enviroment.Model.Card.MagicCard.Buff;

import Enviroment.Model.Ability.Debuff;
import Enviroment.Model.Card.MagicCard.MagicCard;
import Enviroment.Model.Field.Field;
import enums.CardType;
import enums.Raw;

public class DebuffCard implements MagicCard {

    private Debuff debuff;
    final private CardType cardType = CardType.DEBUFFCARD;



    public DebuffCard(){
        debuff = new Debuff();
    }

    public void setUpRaw(Raw raw){
        debuff.setRaw(raw);
    }

    @Override
    public String getName() {
        return "Debuff";
    }

    @Override
    public CardType getCartType() {
        return this.cardType;
    }

    @Override
    public void activateAbility(Field field) {
        debuff.useAbility(field);
    }
}
