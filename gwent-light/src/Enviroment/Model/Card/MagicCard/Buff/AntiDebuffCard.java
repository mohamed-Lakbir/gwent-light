package Enviroment.Model.Card.MagicCard.Buff;

import Enviroment.Model.Ability.AntiDebuffAbillity;
import Enviroment.Model.Card.MagicCard.MagicCard;
import Enviroment.Model.Field.Field;
import enums.CardType;

public class AntiDebuffCard implements MagicCard {
    private AntiDebuffAbillity antiDebuff;
    private final CardType  cardType = CardType.ANTIDEBUFFCARD;
    public AntiDebuffCard(){
        antiDebuff = new AntiDebuffAbillity();
    }

    @Override
    public void activateAbility(Field field) {
        antiDebuff.useAbility(field);
    }

    @Override
    public String getName() {
        return "AntiDebuff";
    }

    @Override
    public CardType getCartType() {
        return cardType;
    }
}
