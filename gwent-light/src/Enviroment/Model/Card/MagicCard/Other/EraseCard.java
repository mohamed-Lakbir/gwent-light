package Enviroment.Model.Card.MagicCard.Other;

import Enviroment.Model.Ability.Erase;
import Enviroment.Model.Card.MagicCard.MagicCard;
import Enviroment.Model.Field.Field;
import enums.CardType;

public class EraseCard implements MagicCard {
    private Erase erase;
    private final String name="Erase";
    private final CardType cardType = CardType.ERASECARD;
    public EraseCard(){
            erase = new Erase();
    }

    @Override
    public void activateAbility(Field field) {
        erase.useAbility(field);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public CardType getCartType() {
        return cardType;
    }
}
