package Enviroment.Model.Card.MagicCard;

import Enviroment.Model.Card.Card;
import Enviroment.Model.Field.Field;

public interface MagicCard extends Card {
    public void activateAbility(Field field);
}
