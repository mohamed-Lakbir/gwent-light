package Enviroment.Model.Card.MonsterCard;

import Enviroment.Model.Card.Card;
import enums.CardType;
import enums.Raw;

public class MonsterCard implements Card {
    private String name;
    private String path;
    private int value;
    private CardType type = CardType.MONSTERCARD;
    private Raw raw;


    public MonsterCard(String name, String path, int value,Raw raw) {
        this.name = name;
        this.path = path;
        this.value = value;
        this.raw = raw;
    }

    public String toString(){
        return "|"+this.name+"|"+"\n"
                +"|"+this.value+"|";
    }

    public void print(){
        System.out.println(toString());
    }


    public Raw getRaw() {
        return raw;
    }

    public void setRaw(Raw raw) {
        this.raw = raw;
    }

    public String getName() {
        return name;
    }

    @Override
    public CardType getCartType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
    public void setCardType(CardType type){
        this.type =type;
    }
    public CardType getType() {
        return type;
    }


}
