package Enviroment.Model.Card.MonsterCard.MagicMonsterCard;

import Enviroment.Model.Ability.PullMore;
import Enviroment.Model.Card.MagicCard.MagicCard;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import Enviroment.Model.Field.Field;
import enums.CardType;
import enums.Raw;

public class PullMoreCard extends MonsterCard implements MagicCard {
    private PullMore ability;
    final private CardType secondType  = CardType.PULLMORECARD;
    public PullMoreCard(int value, Raw raw) {
        super("PullMoreCard", "", value, raw);
        super.setCardType(secondType);
        ability = new PullMore();
    }



    @Override
    public void activateAbility(Field field) {
        ability.useAbility(field);
    }
}
