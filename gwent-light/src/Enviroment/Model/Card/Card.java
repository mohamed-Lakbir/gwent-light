package Enviroment.Model.Card;

import enums.CardType;

public interface Card {
    public String toString();
    public String getName();
    public CardType getCartType();

}
