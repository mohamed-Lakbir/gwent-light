package Enviroment.Model.Card.CardStacks;

import Enviroment.Model.Card.Card;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import enums.CardType;

public class RawStack extends  CardStack {

    private int value;
    private int modifiedValue;
    private  boolean buffed;
    private  boolean deBuffed;
    private Card buffCard;

    public RawStack(){
        this.value = 0;
        this.modifiedValue =0;
        this.buffed = false;
        this.deBuffed = false;
        buffCard = null;
    }

    public void clear(){
        super.clear();
        buffed = false;
        deBuffed = false;
    }

    public  void addBuffCard(Card c){
        if(buffCard.getCartType().equals(CardType.BUFFCARD)) {
            this.buffCard = c;
            calculateModifedValue();
        }else{
            System.err.println("Diese Karte kann hier nicht gesetzt werden.");
        }
    }

    public void calculateValue(){
        this.value =0;
        for(Card c : this.getCards()){
            MonsterCard mc = (MonsterCard) c;
            this.value = this.value + mc.getValue();
        }
    }

    public  void calculateModifedValue(){
        calculateValue();
        modifiedValue = value;
        if(buffed){
            modifiedValue = value*2;
        }
        if(isDeBuffed()){
            modifiedValue = this.getCards().size();
        }
    }

    @Override
    public void add(Card c) {
        if(c.getCartType().equals(CardType.MONSTERCARD)||c.getCartType().equals(CardType.PULLMORECARD)){
            super.add(c);
            calculateModifedValue();
        }else {
            //throwexception
            System.err.println("In einer Raw dürfen nur Monsterkarten gespielt werden. Für Buffkarten gibt es eine extra Methode");
        }
    }

    @Override
    public String toString() {
        String s = "";
        if(this.buffed == true&&deBuffed ==false){
            s =s + "Raw is buffed";
        }
        if(this.deBuffed == true){
            s =s + "Raw is debuffed";
        }
        s = "Points : " + getModifiedValue()+"\n" + s+ super.toString();
        return s;
    }


    public  void print(){
        System.out.println(toString());
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getModifiedValue() {
        calculateModifedValue();
        return modifiedValue;
    }

    public void setModifiedValue(int modifiedValue) {
        this.modifiedValue = modifiedValue;
    }

    public boolean isBuffed() {
        return buffed;
    }

    public void setBuffed(boolean buffed) {
        this.buffed = buffed;
    }

    public boolean isDeBuffed() {
        return deBuffed;
    }

    public void setDeBuffed(boolean deBuffed) {
        this.deBuffed = deBuffed;
    }

    public Card getBuffCard() {
        return buffCard;
    }

    public void setBuffCard(Card buffCard) {
        this.buffCard = buffCard;
    }
}
