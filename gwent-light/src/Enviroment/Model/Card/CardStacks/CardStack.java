package Enviroment.Model.Card.CardStacks;

import Enviroment.Model.Card.Card;
import Enviroment.Model.Card.MagicCard.Buff.BuffCard;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import enums.CardType;

import java.util.LinkedList;
import java.util.Random;

public class CardStack {
    private LinkedList <Card> cards;


    public CardStack(){
        cards = new LinkedList<>();
    }

    public void add(Card c){
        cards.add(c);
    }

    public Card dropCard(String cardName){
        for(Card c : cards){
            if(c.getName().toLowerCase().equals(cardName.toLowerCase())){
                Card temp = c;
                cards.remove(c);
                return temp;
            }
        }
        return null;
    }

    public MonsterCard getLowestCard(){
        MonsterCard lowestCard = null;
        for(Card c : cards){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                if(lowestCard == null){
                    lowestCard = (MonsterCard) c;
                }
                MonsterCard cM = (MonsterCard)c;
                if(cM.getValue()<lowestCard.getValue()){
                    lowestCard = cM;
                }
            }
        }
        if(lowestCard == null){
            String s = "" ;
        }
        return lowestCard;
    }

    public String getLowestCardName(){
        return getLowestCard().getName();
    }



    public boolean anyMonsterCard(){
        for(Card card : cards){
            if(card.getCartType().equals(CardType.MONSTERCARD)){
                return true;
            }
        }
        return false;
    }
    public boolean anyEraseCard(){
        for(Card card : cards){
            if(card.getCartType().equals(CardType.ERASECARD)){
                return true;
            }
        }
        return false;
    }

    public boolean anyDebuffCard(){
        for(Card card : cards){
            if(card.getCartType().equals(CardType.DEBUFFCARD)){
                return true;
            }
        }
        return false;
    }


    public boolean anyBuffCard(){
        for(Card card : cards){
            if(card.getCartType().equals(CardType.BUFFCARD)){
                return true;
            }
        }
        return false;
    }

    public boolean buffOrDebuffCard(String cardName){

        for(Card c : cards){
            if(cardName.equals(c.getName())){
                if(c.getCartType().equals(CardType.BUFFCARD)||c.getCartType().equals(CardType.DEBUFFCARD)){
                    return true;
                }
            }else {
                return false;
            }
        }
        return false;
    }

    public void clear(){
        this.cards.clear();
    }

    public void switchCard(String cardName, CardStack aimStack){
        Card c = this.dropCard(cardName);
        aimStack.add(c);
    }

    public Card dropFirst(){
        return this.cards.getFirst();
    }

    public Card dropLast(){
        Card c = this.getCards().getLast();
        this.getCards().removeLast();
        return c;
    }

    public boolean existCardInDec(String cardName){
        for(Card c : cards){
            if(cardName.toLowerCase().equals(c.getName().toLowerCase())){
                return true;
            }
        }
        return false;
    }


    public String toString(){
        String cardName = "";
        String cardPoints ="";
        MonsterCard mc;
        for(Card c : cards){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                mc= (MonsterCard)c;
                String raw;
                switch (mc.getRaw()){
                    case RAW_ONE -> raw = "Raw One";
                    case RAW_TWO -> raw = "Raw Two";
                    case RAW_THREE -> raw = "Raw Three";
                    default -> throw new IllegalStateException("Unexpected value: " + mc.getRaw());
                }
                cardName = cardName+"|" + c.getName()+": "+mc.getValue()+" #"+ raw +"|";
                //cardPoints = cardPoints +"|"+ String.valueOf(mc.getValue())+"|";
            }
        }
        for(Card c : cards){
            if(!c.getCartType().equals(CardType.MONSTERCARD)){
                cardName = cardName +"|" + c.getName()+"|";
            }
        }
        return cardName +"\n"+cardPoints;
    }


    public void mixCards(){

        CardStack mixedStack = new CardStack();
        Random r = new Random();

        while (cards.size()!= 0){
            Card c = cards.get(r.nextInt(cards.size()));
            cards.remove(c);
            mixedStack.add(c);
        }
        cards = mixedStack.getCards();
    }

    public static CardStack deepCopy (CardStack c){
        CardStack cardStack = new CardStack();

        cardStack.setCards((LinkedList<Card>) c.getCards().clone());
        return cardStack;

    }

    public boolean containsBuffCard(){
        for(Card c : cards){
            if(c.getCartType().equals(CardType.BUFFCARD)){
                return true;
            }
        }
        return false;
    }

    public boolean containsDeBuffCard(){
        for(Card c : cards){
            if(c.getCartType().equals(CardType.DEBUFFCARD)){
                return true;
            }
        }
        return false;
    }

    public boolean containsPullMoreCard(){
        for(Card c : cards){
            if(c.getCartType().equals(CardType.PULLMORECARD)){
                return true;
            }
        }
        return false;
    }

    public boolean containsEraseCard(){
        for(Card c : cards){
            if(c.getCartType().equals(CardType.ERASECARD)){
                return true;
            }
        }
        return false;
    }

    public boolean containsMonsterCard(){
        for(Card c : cards){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                return true;
            }
        }
        return false;
    }

    public boolean containsAntiDebuffCard(){
        for(Card c : cards){
            if(c.getCartType().equals(CardType.ANTIDEBUFFCARD)){
                return true;
            }
        }
        return false;
    }


    private LinkedList<MonsterCard>[] sortMonsterCardsByRaw(){
        LinkedList<MonsterCard> filterdCards []= new LinkedList[3];
        LinkedList<MonsterCard> sortedByValue;
        for(Card c : cards){
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                switch (((MonsterCard)(c)).getRaw()){
                    case RAW_ONE -> filterdCards[0].add((MonsterCard) c);
                    case RAW_THREE -> filterdCards[2].add((MonsterCard) c);
                    case RAW_TWO -> filterdCards[1].add((MonsterCard) c);
                }
            }
        }

        for(LinkedList <MonsterCard>list : filterdCards){
            sortedByValue= new LinkedList<>();
            for(int i=0;i<list.size();i++){
                if(i==0){
                    sortedByValue.add(list.get(i));
                }else{
                    for(int j=0;i<sortedByValue.size();j++){
                        if(list.get(i).getValue()<sortedByValue.get(j).getValue()){
                            sortedByValue.add(j,list.get((i)));
                        }else if(j==sortedByValue.size()-1){
                            sortedByValue.addLast(list.get(i));
                        }
                    }
                }
            }
            list = sortedByValue;
        }
        return filterdCards;
    }

    public  void removeCard ( String cardName){
        this.dropCard(cardName);
    }

    public String cardStackInformation(){
        return "Cards : " + this.cards.size();
    }

    public void printCardStackInfo() {
        System.out.println(cardStackInformation());
    }

    public void print(){

        System.out.println(toString());
    }

    public LinkedList<Card> getCards() {
        return cards;
    }

    public void setCards(LinkedList<Card> cards) {
        this.cards = cards;
    }

}
