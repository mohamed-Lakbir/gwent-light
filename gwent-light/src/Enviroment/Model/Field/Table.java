package Enviroment.Model.Field;

import Enviroment.Model.Card.Card;
import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Card.MagicCard.MagicCard;
import enums.CardType;
import enums.Raw;

public class Table {
    private Field playerOneField;
    private Field playerTwoField;
    private String playerOneName;
    private String playerTwoName;
    //PullMore,Buff,Debuff,AntiDebuff,Erase
    private int []playerOneUsedMagicCards;
    private int []playerTwoUsedMagicCards;

    public Table(){
        playerTwoUsedMagicCards = new int[5];
        playerOneUsedMagicCards = new int[5];
    }

    public Field getOwnField(String name){

        if(playerOneName.equals(name)){
            return playerOneField;
        }
        if(playerTwoName.equals(name)){
            return playerTwoField;
        }

        return null;

    }



    public void setUpTable(CardStack deck,String name){
        if(playerOneField ==null){
            playerOneField = new Field(CardStack.deepCopy(deck));
            playerOneName  = name;
        }else if(playerTwoField == null){
            playerTwoField = new Field(CardStack.deepCopy(deck));
            playerTwoName = name;
        }
    }

    public void mixCards(){
        getPlayerTwoField().getDeck().mixCards();
        getPlayerOneField().getDeck().mixCards();

    }
    public boolean checkIfCardIsInHand(String name,String card){
        if(name.equals(playerOneName)){
            return playerOneField.getHand().existCardInDec(card);
        }else {
            return playerTwoField.getHand().existCardInDec(card);
        }
    }

    public void pullCard(String name,int number){
        if(name.equals(this.playerOneName)){
            playerOneField.pullCards(number);
        }else{
            playerTwoField.pullCards(number);
        }

    }

    public void resetTable(CardStack deckPlayerOne,CardStack deckPlayerTwo){
        playerOneField.clearField(deckPlayerOne);
        playerTwoField.clearField(deckPlayerTwo);
    }

    public void clearGameFields(){
        playerTwoField.clearPlacedCard();
        playerOneField.clearPlacedCard();
    }


    public void playerUseCard(String playerName,String cardName){
        //PlayerOne
        if(this.playerOneName.equals(playerName)){
            Card c = playerOneField.getHand().dropCard(cardName);
            if(c.getCartType().equals(CardType.ERASECARD)){
                playerUseEraseCard(playerOneName);
                playerTwoField.useMagicAbility((MagicCard)c);
            }
            if(c.getCartType().equals(CardType.ANTIDEBUFFCARD)){
                playerUseAntiDebuffCard(playerName);
                playerOneField.useMagicAbility((MagicCard)c);
            }
            if(c.getCartType().equals(CardType.PULLMORECARD)){
                playerOneField.useMagicAbility((MagicCard)c);
                playerUsePullMoreCard(playerName);
                playerTwoField.playerUseCard(c);
            }
            if( c.getCartType().equals(CardType.MONSTERCARD)){

                playerOneField.playerUseCard(c);
            }
        }
        //PlayerTwo
        if(this.playerTwoName.equals(playerName)){
            Card c = playerTwoField.getHand().dropCard(cardName);
            if(c==null){
                System.err.println("Error");
            }
            if(c.getCartType().equals(CardType.ERASECARD)){
                playerUseEraseCard(playerOneName);
                playerOneField.playerUseCard(c);
            }
            if(c.getCartType().equals(CardType.ANTIDEBUFFCARD)){
                playerUseAntiDebuffCard(playerName);
                playerTwoField.useMagicAbility((MagicCard)c);
            }
            if(c.getCartType().equals(CardType.PULLMORECARD)){
                playerUsePullMoreCard(playerName);
                playerTwoField.useMagicAbility((MagicCard)c);
                playerOneField.playerUseCard(c);
            }
            if(c.getCartType().equals(CardType.MONSTERCARD)){
                playerTwoField.playerUseCard(c);
            }
        }
    }

    public void playerUseCard(String playerName,String cardName,Raw raw){
        if(this.playerOneName.equals(playerName)){
            Card c = playerOneField.getHand().dropCard(cardName);
            if(c.getCartType().equals(CardType.DEBUFFCARD)){
                playerUseDebuffCard(playerName);
                playerTwoField.playerUseCard(c,raw);
            }
            if(c.getCartType().equals(CardType.BUFFCARD)){
                playerUseBuffCard(playerName);
                playerOneField.playerUseCard(c,raw);
            }
        }
        if(this.playerTwoName.equals(playerName)){
            Card c = playerTwoField.getHand().dropCard(cardName);
            if(c.getCartType().equals(CardType.DEBUFFCARD)){
                playerOneField.playerUseCard(c,raw);
            }
            if(c.getCartType().equals(CardType.BUFFCARD)){
                playerTwoField.playerUseCard(c,raw);
            }
        }
    }




    public int getValue(String name){
        if(this.playerOneName.equals(name)){
            return playerOneField.value();
        }
        if(this.playerTwoName.equals(name)){
            return playerTwoField.value();
        }
        return 0;
    }
    //PullMore,Buff,Debuff,AntiDebuff,Erase

    public int getNumberOfOponnentUsedPullMoreCard(String playerName){
        if(!playerName.equals(playerTwoName)){
            return playerTwoUsedMagicCards[0];
        }else {
            return playerOneUsedMagicCards[0];
        }
    }


    public int getNumberOfOponnentUsedBuffCard(String playerName){
        if(!playerName.equals(playerTwoName)){
            return playerTwoUsedMagicCards[1];
        }else {
            return playerOneUsedMagicCards[1]++;
        }
    }
    public int getNumberOfOponnentUsedDebuffCard(String playerName){
        if(!playerName.equals(playerTwoName)){
            return playerTwoUsedMagicCards[2];
        }else {
            return playerOneUsedMagicCards[2]++;
        }
    }
    public int getNumberOfOponnentUsedAntiDebuffCard(String playerName){
        if(!playerName.equals(playerTwoName)){
            return playerTwoUsedMagicCards[3];
        }else {
            return playerOneUsedMagicCards[3]++;
        }
    }

    public int getNumberOfOponnentUsedEraseCard(String playerName){
        if(!playerName.equals(playerTwoName)){
            return playerTwoUsedMagicCards[4];
        }else {
            return playerOneUsedMagicCards[4]++;
        }
    }




    public void playerUsePullMoreCard(String playerName){
        if(playerName.equals(playerTwoName)){
            playerTwoUsedMagicCards[0]++;
        }else {
            playerOneUsedMagicCards[0]++;
        }
    }


    public void playerUseBuffCard(String playerName){
        if(playerName.equals(playerTwoName)){
            playerTwoUsedMagicCards[1]++;
        }else {
            playerOneUsedMagicCards[1]++;
        }
    }



    public void playerUseDebuffCard(String playerName){
        if(playerName.equals(playerTwoName)){
            playerTwoUsedMagicCards[2]++;
        }else {
            playerOneUsedMagicCards[2]++;
        }
    }



    public void playerUseAntiDebuffCard(String playerName){
        if(playerName.equals(playerTwoName)){
            playerTwoUsedMagicCards[3]++;
        }else {
            playerOneUsedMagicCards[3]++;
        }
    }


    public void playerUseEraseCard(String playerName){
        if(playerName.equals(playerTwoName)){
            playerTwoUsedMagicCards[4]++;
        }else {
            playerOneUsedMagicCards[4]++;
        }
    }

    public Field getOpponentField(String ownName){
        if(playerOneName.equals(ownName)){
            return playerTwoField;
        }
        if(playerTwoName.equals(ownName)){
            return playerTwoField;
        }

        return null;

    }


    public void printTable(){
        playerTwoField.printReversed();
        System.out.println("--------------------------------------------------------------------------------------");
        playerOneField.print();
    }


    public Field getPlayerOneField() {
        return playerOneField;
    }

    public void setPlayerOneField(Field playerOneField) {
        this.playerOneField = playerOneField;
    }

    public Field getPlayerTwoField() {
        return playerTwoField;
    }

    public void setPlayerTwoField(Field playerTwoField) {
        this.playerTwoField = playerTwoField;
    }

    public String getPlayerOneName() {
        return playerOneName;
    }

    public void setPlayerOneName(String playerOneName) {
        this.playerOneName = playerOneName;
    }

    public String getPlayerTwoName() {
        return playerTwoName;
    }

    public void setPlayerTwoName(String playerTwoName) {
        this.playerTwoName = playerTwoName;
    }
}
