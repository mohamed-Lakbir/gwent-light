package Enviroment.Model.Field;

import Enviroment.Model.Card.Card;
import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Card.CardStacks.RawStack;
import Enviroment.Model.Card.MagicCard.Buff.BuffCard;
import Enviroment.Model.Card.MagicCard.Buff.DebuffCard;
import Enviroment.Model.Card.MagicCard.MagicCard;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import enums.CardType;
import enums.Raw;

import java.util.LinkedList;

public class Field {
    private CardStack deck;
    private CardStack hand;
    private RawStack firstRawStack;
    private RawStack secondRawStack;
    private RawStack thirdRawStack;

    public  Field(CardStack deck){
        this.deck = CardStack.deepCopy(deck);
        hand = new CardStack();
        firstRawStack = new RawStack();
        secondRawStack = new RawStack();
        thirdRawStack = new RawStack();
    }

    public void clearPlacedCard(){
        firstRawStack.clear();
        secondRawStack.clear();
        thirdRawStack.clear();


    }


    public int value(){

        return firstRawStack.getModifiedValue() + secondRawStack.getModifiedValue() + thirdRawStack.getModifiedValue();
    }

    public void clearHand(){
        hand.clear();
    }


    public void clearField(CardStack deck){
        playerNewDeck(deck);
        clearPlacedCard();
        clearHand();

    }

    public void playerNewDeck(CardStack deck){
        this.deck = CardStack.deepCopy(deck);
    }

    public void pullCards(int number){
        for(int i =0;i<number&&i<deck.getCards().size();i++){
            this.hand.add(this.deck.dropLast());
        }

    }
    public void playerUseCard(Card card){
        if(card.getCartType().equals(CardType.MONSTERCARD)||card.getCartType().equals(CardType.PULLMORECARD)){
            MonsterCard cM = (MonsterCard) card;
            switch (cM.getRaw()){
                case RAW_ONE -> this.firstRawStack.add(card);
                case RAW_TWO -> this.secondRawStack.add(card);
                case RAW_THREE -> this.thirdRawStack.add(card);
            }
        }
    }

    public void useMagicAbility(MagicCard card){
        card.activateAbility(this);
    }

    public void playerUseCard(Card card, Raw raw){
        if(card.getCartType().equals(CardType.BUFFCARD)){
            BuffCard cb = (BuffCard) card;
            cb.setUpRaw(raw);
            cb.activateAbility(this);

        }
        if(card.getCartType().equals(CardType.DEBUFFCARD)){
            DebuffCard cb = (DebuffCard) card;
            cb.setUpRaw(raw);
            cb.activateAbility(this);
        }
    }


    public boolean checkIfRawIsDebuffes(Raw raw){
        switch (raw){
            case RAW_ONE -> {
                return firstRawStack.isDeBuffed();
            }
            case RAW_TWO -> {
                return secondRawStack.isDeBuffed();
            }
            case RAW_THREE -> {
                return thirdRawStack.isDeBuffed();
            }
        }
        return false;
    }
/*
    public void playerUsedCard(String cardName){
        Card c = this.getHand().dropCard(cardName);
        if(c.getCartType().equals(CardType.MONSTERCARD)){
            if(c.getClass().equals(PullMoreCard.class)){
                ((PullMoreCard)c).activateAbility(this);
            }
            MonsterCard cM = (MonsterCard) c;
            switch (cM.getRaw()){
                case RAW_ONE -> firstRawStack.add(c);
                case RAW_TWO -> secondRawStack.add(c);
                case RAW_THREE -> thirdRawStack.add(c);
            }
        }
        if(c.getCartType().equals(CardType.ERASECARD)){
            ((EraseCard)c).activateAbility(this);
        }
    }

    public void playerUsedCard(String cardName, Raw raw){
        Card c = this.getHand().dropCard(cardName);
        if(c.getCartType().equals(CardType.BUFFCARD)){
            BuffCard cb = (BuffCard) c;
            cb.setUpRaw(raw);
            cb.activateAbility(this);

        }
        if(c.getCartType().equals(CardType.DEBUFFCARD)){
            DebuffCard cb = (DebuffCard) c;
            cb.setUpRaw(raw);
            cb.activateAbility(this);
        }
    }
*/
    public String reverseToString(){
        return
                "Field Value : " + value() + "\n"+
                "Third raw : " + thirdRawStack.toString()+"\n"+
                "Second raw : "+ secondRawStack.toString() + "\n"+
                "FIrst raw : "+ firstRawStack.toString() + "\n"+
                "Hand = "+hand.cardStackInformation() + " Deck "+ this.deck.cardStackInformation()+"\n";
    }

    public void printReversed(){
        System.out.println(reverseToString());
    }


    public String toString(){
         return "Field Value : " + value() + "\n"+
                 "First raw : "+ firstRawStack.toString() + "\n"+
                 "Second raw : "+ secondRawStack.toString() + "\n"+
                 "Third raw : " + thirdRawStack.toString()+"\n"+
                 "Hand : \n" + hand.toString() +  "Deck "+ this.deck.cardStackInformation()+"\n";
    }

    public void print (){
        System.out.println(toString());
    }

    public CardStack getDeck() {
        return deck;
    }

    public void setDeck(CardStack deck) {
        this.deck = deck;
    }

    public CardStack getHand() {
        return hand;
    }

    public void setHand(CardStack hand) {
        this.hand = hand;
    }

    public RawStack getFirstRawStack() {
        return firstRawStack;
    }

    public void setFirstRawStack(RawStack firstRawStack) {
        this.firstRawStack = firstRawStack;
    }

    public RawStack getSecondRawStack() {
        return secondRawStack;
    }

    public void setSecondRawStack(RawStack secondRawStack) {
        this.secondRawStack = secondRawStack;
    }

    public RawStack getThirdRawStack() {
        return thirdRawStack;
    }

    public void setThirdRawStack(RawStack thirdRawStack) {
        this.thirdRawStack = thirdRawStack;
    }
}
