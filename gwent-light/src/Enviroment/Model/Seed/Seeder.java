package Enviroment.Model.Seed;

import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Card.MagicCard.Buff.AntiDebuffCard;
import Enviroment.Model.Card.MagicCard.Buff.BuffCard;
import Enviroment.Model.Card.MagicCard.Buff.DebuffCard;
import Enviroment.Model.Card.MagicCard.Other.EraseCard;
import Enviroment.Model.Card.MonsterCard.MagicMonsterCard.PullMoreCard;
import Enviroment.Model.Card.MonsterCard.MonsterCard;
import enums.Raw;

import java.util.Random;

public class Seeder {

    public static CardStack createMonsterCardPlayer(){
        CardStack deck = new CardStack();
        Random r = new Random();
        String names[] = {
                "Magokai","Sasbuke","Narubuto","Yambas","Handu",
                "Gerald","Yenni","Triess","Vladimir","Zuko"
                ,"Cloud","Handa","Ryus","Kenny","Sam",
                "Luchs","Annie","Voltbär","Sion","Anivias",
                "Yand","Goog","Xi","Fakebook","Abc",
                "Rubas","Subsa","Janso","Hida","Zuhuras"
        };
        int random =0;
        for(int i=0;i<20;i++){
            Raw raw;
            int a  =new Random().nextInt(3);
            switch (a){
                case 0:
                    raw = Raw.RAW_ONE;
                    break;
                case 1:
                    raw = Raw.RAW_TWO;
                    break;
                case 2:
                    raw = Raw.RAW_THREE;
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + a);
            }
            random = r.nextInt(10)+1;
            deck.add(new MonsterCard(names[i],"",random,raw));
        }
        return deck;
    }

    public static CardStack createAllgmightDeck(){
            CardStack deck = new CardStack();
            Random r = new Random();
            int random =0;
            for(int i=0;i<30;i++){
                Raw raw;
                int a  =new Random().nextInt(3);
                switch (a){
                    case 0:
                        raw = Raw.RAW_ONE;
                        break;
                    case 1:
                        raw = Raw.RAW_TWO;
                        break;
                    case 2:
                        raw = Raw.RAW_THREE;
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + a);
                }
                random = r.nextInt(10);
                deck.add(new MonsterCard("Allgmight","",1000000000,raw));
            }
            return deck;
        }
    public static CardStack createDeckWithBuffCard(){
        CardStack cardStack = createMonsterCardPlayer();
        BuffCard buffCard = new BuffCard();
        DebuffCard debuffCard = new DebuffCard();
        cardStack.add(buffCard);
        cardStack.add(debuffCard);
        cardStack.add(new EraseCard());
        cardStack.add(new PullMoreCard(3, Raw.RAW_ONE));
        return cardStack;
    }

    public static CardStack createRegularDeck(){
        CardStack cardStack = createMonsterCardPlayer();
        Random r = new Random();
        for(int i =0;i<2;i++){

            cardStack.add(new BuffCard());
            cardStack.add(new DebuffCard());
            cardStack.add(new EraseCard());
            cardStack.add(new AntiDebuffCard());
            switch (r.nextInt(3)){
                case 0:
                    cardStack.add(new PullMoreCard(3,Raw.RAW_ONE));
                    break;
                case 1:
                    cardStack.add(new PullMoreCard(3, Raw.RAW_TWO));
                    break;
                case 2:
                    cardStack.add(new PullMoreCard(3, Raw.RAW_THREE));
                    break;
            }

        }
        cardStack.mixCards();

        return cardStack;

    }

    public static CardStack createRegularDeck2(){
        CardStack cardStack = createMonsterCardPlayer();
        Random r = new Random();
        for(int i =0;i<2;i++){

            cardStack.add(new BuffCard());
            cardStack.add(new DebuffCard());
            cardStack.add(new EraseCard());
            cardStack.add(new AntiDebuffCard());
            switch (r.nextInt(3)){
                case 0:
                    cardStack.add(new PullMoreCard(3,Raw.RAW_ONE));
                    break;
                case 1:
                    cardStack.add(new PullMoreCard(3, Raw.RAW_TWO));
                    break;
                case 2:
                    cardStack.add(new PullMoreCard(3, Raw.RAW_THREE));
                    break;
            }

        }
        cardStack.mixCards();
        return cardStack;

    }








}
