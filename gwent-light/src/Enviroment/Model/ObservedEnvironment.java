package Enviroment.Model;

import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Field.Field;
import Enviroment.Model.Field.Table;
import Enviroment.Model.Game.Game;

public class ObservedEnvironment {

    private Table table;

    private int [] agentRawValue;
    private boolean [] agentRawBuffed;
    private boolean [] agentRawDebuffed;
    private int agentFieldValue;
    private boolean agentPass;
    private int agentWins;
    private CardStack agentHand;
    private int agentDeck;

    private int [] opponentRawValue;
    private boolean [] opponentRawBuffed;
    private boolean [] opponentRawDebuffed;
    private int opponentFieldValue;
    private boolean opponentPass;
    private int opponentWins;
    private int opponentHand;
    private int opponentDeck;

    private int oponnentPlayedBuffCard;
    private int oponnentPlayedEraseCard;
    private int oponnentPlayedDebuffCard;
    private int oponnentPlayedPullMoreCard;
    private int oponnentPlayedAntiDebuffCard;



    private int games;


    public void createState(Game environment, String name){
        games = environment.getGames();
        this.table = environment.getTable();
        if(environment.getPlayerOne().getName().equals(name)){

            this.opponentDeck = environment.getPlayerTwo().getDeck().getCards().size();
            this.opponentHand = environment.getPlayerTwo().getTable().getPlayerTwoField().getHand().getCards().size();
            this.opponentPass = environment.getPlayerTwo().isPass();
            this.opponentWins = environment.getPlayerTwo().getWins();
            this.opponentRawValue = new int[]{environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().getValue(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().getValue(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().getValue()};
            this.opponentRawBuffed = new boolean[]{environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().isBuffed(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getSecondRawStack().isBuffed(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getThirdRawStack().isBuffed()};
            this.opponentRawDebuffed = new boolean[]{environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().isDeBuffed(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getSecondRawStack().isDeBuffed(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getThirdRawStack().isDeBuffed()};

            this.opponentFieldValue = environment.getPlayerTwo().getTable().getPlayerTwoField().value();

            this.agentPass = environment.getPlayerOne().isPass();
            this.agentWins = environment.getPlayerOne().getWins();
            this.agentHand = environment.getPlayerOne().getTable().getPlayerOneField().getHand();
            this.agentDeck = environment.getPlayerOne().getTable().getPlayerOneField().getDeck().getCards().size();
            this.agentRawValue = new int[]{environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().getValue(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().getValue(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().getValue()};
            this.agentRawBuffed = new boolean[]{environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().isBuffed(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getSecondRawStack().isBuffed(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getThirdRawStack().isBuffed()};
            this.agentRawDebuffed = new boolean[]{environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().isDeBuffed(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getSecondRawStack().isDeBuffed(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getThirdRawStack().isDeBuffed()};

            this.agentFieldValue = environment.getPlayerOne().getTable().getPlayerOneField().value();

        }else if(environment.getPlayerTwo().getName().equals(name)){
            this.opponentDeck = environment.getPlayerOne().getDeck().getCards().size();
            this.opponentHand = environment.getPlayerOne().getTable().getPlayerTwoField().getHand().getCards().size();
            this.opponentPass = environment.getPlayerOne().isPass();
            this.opponentWins = environment.getPlayerOne().getWins();

            this.opponentRawValue = new int[]{environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().getValue(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().getValue(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().getValue()};
            this.opponentRawBuffed = new boolean[]{environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().isBuffed(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getSecondRawStack().isBuffed(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getThirdRawStack().isBuffed()};
            this.opponentRawDebuffed = new boolean[]{environment.getPlayerOne().getTable().getPlayerOneField().getFirstRawStack().isDeBuffed(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getSecondRawStack().isDeBuffed(),
                    environment.getPlayerOne().getTable().getPlayerOneField().getThirdRawStack().isDeBuffed()};

            this.opponentFieldValue = environment.getPlayerOne().getTable().getPlayerOneField().value();

            this.agentPass = environment.getPlayerTwo().isPass();
            this.agentWins = environment.getPlayerTwo().getWins();
            this.agentHand = environment.getPlayerTwo().getTable().getPlayerTwoField().getHand();
            this.agentDeck = environment.getPlayerTwo().getTable().getPlayerTwoField().getDeck().getCards().size();

            this.agentRawValue = new int[]{environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().getValue(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().getValue(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().getValue()};
            this.agentRawBuffed = new boolean[]{environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().isBuffed(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getSecondRawStack().isBuffed(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getThirdRawStack().isBuffed()};
            this.agentRawDebuffed = new boolean[]{environment.getPlayerTwo().getTable().getPlayerTwoField().getFirstRawStack().isDeBuffed(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getSecondRawStack().isDeBuffed(),
                    environment.getPlayerTwo().getTable().getPlayerTwoField().getThirdRawStack().isDeBuffed()};

            this.agentFieldValue = environment.getPlayerTwo().getTable().getPlayerTwoField().value();
        }
    }


    public void resetGame(){

    }

    public int calculateHighest(int array[]){
        int highestRaw =0;
        for(int i=0;i<array.length;i++){
            if((array[i]>array[highestRaw])){
                highestRaw =i;
            }
        }
        return highestRaw;
    }

    public int calculateHighestOpponentRaw(){
        return calculateHighest(opponentRawValue);
    }
    public int calculateHighestAgentRaw(){
        return calculateHighest(agentRawValue);
    }



    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public boolean isAgentPass() {
        return agentPass;
    }

    public void setAgentPass(boolean agentPass) {
        this.agentPass = agentPass;
    }

    public int getAgentWins() {
        return agentWins;
    }

    public void setAgentWins(int agentWins) {
        this.agentWins = agentWins;
    }

    public CardStack getAgentHand() {
        return agentHand;
    }

    public void setAgentHand(CardStack agentHand) {
        this.agentHand = agentHand;
    }

    public int getAgentDeck() {
        return agentDeck;
    }

    public void setAgentDeck(int agentDeck) {
        this.agentDeck = agentDeck;
    }


    public boolean isOpponentPass() {
        return opponentPass;
    }

    public void setOpponentPass(boolean opponentPass) {
        this.opponentPass = opponentPass;
    }

    public int getOpponentWins() {
        return opponentWins;
    }

    public void setOpponentWins(int opponentWins) {
        this.opponentWins = opponentWins;
    }

    public int getOpponentHand() {
        return opponentHand;
    }

    public void setOpponentHand(int opponentHand) {
        this.opponentHand = opponentHand;
    }

    public int getOpponentDeck() {
        return opponentDeck;
    }

    public void setOpponentDeck(int opponentDeck) {
        this.opponentDeck = opponentDeck;
    }

    public int[] getAgentRawValue() {
        return agentRawValue;
    }

    public void setAgentRawValue(int[] agentRawValue) {
        this.agentRawValue = agentRawValue;
    }

    public boolean[] getAgentRawBuffed() {
        return agentRawBuffed;
    }

    public void setAgentRawBuffed(boolean[] agentRawBuffed) {
        this.agentRawBuffed = agentRawBuffed;
    }

    public boolean[] getAgentRawDebuffed() {

        return agentRawDebuffed;
    }

    public void setAgentRawDebuffed(boolean[] agentRawDebuffed) {
        this.agentRawDebuffed = agentRawDebuffed;
    }

    public int getAgentFieldValue() {
        return agentFieldValue;
    }

    public void setAgentFieldValue(int agentFieldValue) {
        this.agentFieldValue = agentFieldValue;
    }

    public int[] getOpponentRawValue() {
        return opponentRawValue;
    }

    public void setOpponentRawValue(int[] opponentRawValue) {
        this.opponentRawValue = opponentRawValue;
    }

    public boolean[] getOpponentRawBuffed() {
        return opponentRawBuffed;
    }

    public void setOpponentRawBuffed(boolean[] opponentRawBuffed) {
        this.opponentRawBuffed = opponentRawBuffed;
    }

    public boolean[] getOpponentRawDebuffed() {
        return opponentRawDebuffed;
    }

    public void setOpponentRawDebuffed(boolean[] opponentRawDebuffed) {
        this.opponentRawDebuffed = opponentRawDebuffed;
    }

    public int getOpponentFieldValue() {
        return opponentFieldValue;
    }

    public void setOpponentFieldValue(int opponentFieldValue) {
        this.opponentFieldValue = opponentFieldValue;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}
