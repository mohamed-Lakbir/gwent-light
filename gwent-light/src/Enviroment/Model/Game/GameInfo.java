package Enviroment.Model.Game;

import java.util.LinkedList;

public class GameInfo {

    private String status;
    private Game game;


    LinkedList<String>message;

    public GameInfo(Game game){
        message = new LinkedList<>();
        this.game = game;
    }

    public boolean isMyTurn(String playerName){
        if(game.getPlayerOne().getName().equals(playerName)){
            return game.getPlayerOne().isTurn();
        }
        if(game.getPlayerTwo().getName().equals(playerName)){
            return game.getPlayerTwo().isTurn();
        }
        return false;

    }


    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getPlayerTurn(){
        return game.whoTurnToString() +"turn";
    }


    public LinkedList<String> getMessage() {
        return message;
    }

    public void setMessage(LinkedList<String> message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }


    public void printAllInformation(){
        System.out.println("--------------------------------------------------------------\n"
                + game.playerString() +"\n"
                +"-------------------------------------------------------------\n"
                +getField()
                +"-------------------------------------------------------------\n");
    }


    public String getField(){
        return
                    game.getTable().getPlayerTwoName()+":"+"\n"
                +   game.getTable().getPlayerTwoField().reverseToString()
                +   "\n-------------------------------------------------------------------------------"
                +   "\n"+game.getPlayerOne().getName()+": " + "\n"
                +   game.getTable().getPlayerOneField().toString();
    }


}
