package Enviroment.Model.Game;

import Agent.Agent;
import Enviroment.Controller.ConsoleController.PlayerController;
import Enviroment.Model.Player.Player;
import Enviroment.Model.Seed.Seeder;
import Enviroment.View.Frame;
import Agent.HumanAgent;

import javax.swing.text.View;
import java.util.Scanner;

public class MainSystem {
    private Game game;
    private Agent agentOne;
    private Agent agentTwo;
    private GameInfo gameInfo;
    private GameStats gameStats;
    private int games;
    public MainSystem(Agent agentOne, Agent agentTwo){
        agentOne.setPlayerNumber("Player one ");
        agentTwo.setPlayerNumber("Player two ");
        Player playerOne = new Player(agentOne.getName());
        Player playerTwo = new Player(agentTwo.getName());
        this.gameStats = new GameStats(agentOne.getName(),agentTwo.getName());
        this.game = new Game(playerOne,playerTwo);
        this.gameInfo = new GameInfo(game);
        this.agentOne = agentOne;
        this.agentTwo = agentTwo;

        agentOne.getController().setPlayer(game.getPlayerOne());
        agentTwo.getController().setPlayer(game.getPlayerTwo());
        game.seedRegularRandomDeck();
    }
    public MainSystem(Frame frame, Agent agentTwo){
        this.agentOne = new HumanAgent();
        agentOne.setPlayerNumber("Player one ");
        agentTwo.setPlayerNumber("Player two ");
        Player playerOne = new Player(agentOne.getName());
        Player playerTwo = new Player(agentTwo.getName());
        this.gameStats = new GameStats(agentOne.getName(),agentTwo.getName());
        this.game = new Game(playerOne,playerTwo);
        this.gameInfo = new GameInfo(game);
        this.agentTwo = agentTwo;

        agentOne.getController().setPlayer(game.getPlayerOne());
        agentTwo.getController().setPlayer(game.getPlayerTwo());
        game.seedRegularRandomDeck();
        frame = new Frame(((HumanAgent)(agentOne)).getObservedEnvironment(),agentOne.getController());
        this.startHumanViewGame();

    }
    public void startGame(){
        game.resetPlayer();
        game.initialPlayerPull();
        while (!game.checkForGameEnd()){
            game.decideTurn();
            do{
                if(game.getPlayerOne().isTurn()&&game.getTable().getPlayerOneField().getHand().getCards().size()>0){
                    agentOne.myTurn(game);
                }else if(game.getPlayerTwo().isTurn()&&game.getTable().getPlayerTwoField().getHand().getCards().size()>0){
                    agentTwo.myTurn(game);
                }
                game.switchTurn();
            }while (!game.checkForRoundEnd());
            game.calculateRoundWinner();
            game.restartRound();
        }
        if(game.gameEndsInDraw()){
            gameStats.addDraw();
        }else{
            gameStats.addGameWins(game.getGameWinner());
        }
    }
    public void startGameDebug(){
        game.resetPlayer();
        game.initialPlayerPull();
        int numberOfMoves =0;
        while (!game.checkForGameEnd()){
            game.decideTurn();
            do{
                System.out.println("Number of Moves : " + numberOfMoves);
                if(game.getPlayerOne().isTurn()&&game.getTable().getPlayerOneField().getHand().getCards().size()>0){
                    gameInfo.printAllInformation(); //Debug Ausgabe
                    System.out.println(game.getPlayerOne().getName() + " is on turn");//Debug Ausgabe
                    agentOne.myTurn(game);
                }else if(game.getPlayerTwo().isTurn()&&game.getTable().getPlayerTwoField().getHand().getCards().size()>0){
                    gameInfo.printAllInformation(); //Debug Ausgabe
                    System.out.println(game.getPlayerTwo().getName() + " is on turn");//Debug Ausgabe
                    agentTwo.myTurn(game);
                }
                numberOfMoves++;
                game.switchTurn();
            }while (!game.checkForRoundEnd());
            game.calculateRoundWinner();
            game.print();
            game.restartRound();
        }
        if(game.getPlayerTwo().getWins()!=0){
            System.out.println("PlayerTwo wins");
        }
    }
    public void startHumanGame(){
      //  System.out.println("Spiel startet. Bitte Rechtschreibfehler ignorieren :)");
      //  System.out.println("Du spielst jetzt gegen meinen stärksten BOT!!!!!!");
        game.getPlayerOne().setDeck(Seeder.createRegularDeck());
        game.resetPlayer();
        game.initialPlayerPull();
        while (!game.checkForGameEnd()){
            game.decideTurn();
            do{
                if(game.getPlayerOne().isTurn()&&game.getTable().getPlayerOneField().getHand().getCards().size()>0){
                    gameInfo.printAllInformation(); //Debug Ausgabe
                    System.out.println(game.getPlayerOne().getName() + " is on turn");//Debug Ausgabe
                    agentOne.myTurn(game);
                }else if(game.getPlayerTwo().isTurn()&&game.getTable().getPlayerTwoField().getHand().getCards().size()>0){
                    gameInfo.printAllInformation(); //Debug Ausgabe
                    System.out.println("Write an any letter to start the opponent turn");
                    new Scanner(System.in).next();
                    System.out.println(game.getPlayerTwo().getName() + " is on turn");//Debug Ausgabe
                    agentTwo.myTurn(game);
                }
                game.switchTurn();
            }while (!game.checkForRoundEnd());
            game.calculateRoundWinner();
            //gameInfo.printAllInformation();
            game.print();
            game.restartRound();
        }       /* if(game.getPlayerTwo().getWins()!=0){

            System.out.println("Wieder deine Ehre nehmen    - Gar kein Bock");
        }*/
        System.out.println("Vielen Dank für das Spielen \n");
    }

        public void startHumanViewGame(){
            //  System.out.println("Spiel startet. Bitte Rechtschreibfehler ignorieren :)");
            //  System.out.println("Du spielst jetzt gegen meinen stärksten BOT!!!!!!");
            game.getPlayerOne().setDeck(Seeder.createRegularDeck());
            game.resetPlayer();
            game.initialPlayerPull();
            while (!game.checkForGameEnd()){
                game.decideTurn();
                do{
                 if(game.getPlayerTwo().isTurn()&&game.getTable().getPlayerTwoField().getHand().getCards().size()>0){
//                   gameInfo.printAllInformation(); //Debug Ausgabe
                     System.out.println(game.getPlayerTwo().getName() + " is on turn");//Debug Ausgabe
                     agentTwo.myTurn(game);
                     game.switchTurn();
                     System.out.println("Human Agent is on turn");
                    }
                }while (!game.checkForRoundEnd());
                game.calculateRoundWinner();
                game.print();
                game.restartRound();
            }
    }

    public void games(int numberOfGames){
        System.out.println("Sequenz of Games Starts");
        System.out.println("Loading : ");
        for(int i=0;i<numberOfGames;i++){
            if(i%10000==0){
                System.out.print("#");
            }
            startGame();
        }
        System.out.println("\n Game ends with : \n");
        gameStats.printStats();
        System.out.println("----------------------------------------------------------------------------------------------------");
    }


    public void gamesWithSwitchedDeck(int numberOfGames){
        System.out.println("Sequenz of Games Starts");
        System.out.println("Loading : ");
        for(int i=0;i<numberOfGames;i++){
            if(i%10000==0){
                System.out.print("#");
            }
            startGame();
        }
        System.out.println("\n Game ends with : \n");
        gameStats.printStats();
        System.out.println("----------------------------------------------------------------------------------------------------");
    }
}
