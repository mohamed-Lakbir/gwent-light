package Enviroment.Model.Game;

public class GameStats {
    public String playerOneName;
    public String playerTwoName;
    public int playerOneWins;
    public int playerTwoWins;
    public int draws;


    public GameStats(String playerOneName,String playerTwoName){
        this.playerOneName = playerOneName;
        this.playerTwoName = playerTwoName;
    }


    public void addGameWins(String winnerName){
        if(winnerName.equals(playerOneName)){
            playerOneWins++;
        }
        if(winnerName.equals(playerTwoName)){
            playerTwoWins++;
        }
    }

    public void addDraw(){
        draws++;
    }

    public void printStats(){
        System.out.println(toString());
    }



    public String toString(){
        return playerOneName + " wins : "+playerOneWins +"\n"
                +playerTwoName + " wins : "+playerTwoWins +"\n"
                +"Draws : " + draws;
    }










}
