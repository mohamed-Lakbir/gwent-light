package Enviroment.Model.Game;

import Enviroment.Model.Card.CardStacks.CardStack;
import Enviroment.Model.Field.Table;
import Enviroment.Model.Player.Player;
import Enviroment.Model.Seed.Seeder;

import java.util.Random;

public class Game {

    private Player playerOne;
    private Player playerTwo;
    private Table table;
    private int games;

    public Game(Player playerOne,Player playerTwo){
        this.playerOne =playerOne;
        this.playerTwo = playerTwo;
        this.table = new Table();
        playerOne.setNextTable(table);
        playerTwo.setNextTable(table);
        games =0;
    }


    public void setPlayer(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }
        public void restartGame(){
        table.resetTable(playerOne.getDeck(),playerTwo.getDeck());
    }




    public void restartRound()
    {
        table.clearGameFields();
    }


    public void resetPlayer(){
        playerOne.reset();
        playerTwo.reset();
        table = new Table();
        playerOne.setUpGame(table);
        playerTwo.setUpGame(table);
    }

    public void calculateRoundWinner(){
        int p1 = playerTwo.getValue();
        int p2 = playerOne.getValue();
        if(playerTwo.getValue()>playerOne.getValue()){
            playerTwo.win();
        }
        if(playerTwo.getValue()<playerOne.getValue()){
            playerOne.win();
        }
        if(playerTwo.getValue()==playerOne.getValue()){
            playerTwo.win();
            playerOne.win();
        }
        games++;
        playerOne.setPass(false);
        playerTwo.setPass(false);
    }


    public boolean checkForRoundEnd(){
        if(playerOne.isPass()&&playerTwo.isPass()){
            games++;
            return true;
        }
        return false;
    }

    public boolean checkForGameEnd(){
        if(playerOne.getWins()>=2&&playerTwo.getWins()!=2){
            return true;
        }
        if(playerTwo.getWins()>=2&&playerOne.getWins()!=2){
            return true;
        }
        if(playerOne.getDraws()==3){
            return true;
        }
        return false;
    }


    public void clearTable(){
        table = new Table();
    }

    public String whoTurnToString(){

        if(playerTwo.isTurn()){
            return playerTwo.getName();
        }
        if(playerOne.isTurn()){
            return playerOne.getName();
        }
        System.err.println("Something went wrong in the whoTurnToString in class Game");
        return "something went Wrong";
    }



    public String getGameWinner(){
        if(playerOne.getWins()<playerTwo.getWins()){
            return playerTwo.getName();
        }
        if(playerOne.getWins()>playerTwo.getWins()){
            return playerOne.getName();
        }
        if(playerOne.getWins()==playerTwo.getWins()){
            return "Game ends in Draw";
        }
        return "No winner";
    }




    public void switchTurn(){
        if(playerOne.isTurn()||playerOne.isPass()){
            playerTwo.setTurn(true);
            playerOne.setTurn(false);}
        else if(playerTwo.isTurn()||playerTwo.isPass()){
            playerTwo.setTurn(false);
            playerOne.setTurn(true);
        }
        if(playerOne.getTable().getPlayerOneField().getHand().getCards().size()==0){
            playerOne.passRound();
        }
        if(playerTwo.getTable().getPlayerTwoField().getHand().getCards().size()==0){
            playerTwo.passRound();
        }


    }


    public void seedPlayerRandomMonsterDeck(){
        playerOne.setDeck(Seeder.createDeckWithBuffCard());
        playerTwo.setDeck(Seeder.createDeckWithBuffCard());
    }

    public void seedRegularRandomDeck(){
        playerOne.setDeck(Seeder.createRegularDeck());
        playerTwo.setDeck(Seeder.createRegularDeck());
    }

    public void decideTurn(){

        int r = new Random().nextInt();
        if(r%2==0){
            playerOne.setTurn(true);
        }else{
            playerTwo.setTurn(true);
        }

    }


    public void initialPlayerPull(){
        playerOne.getTable().mixCards();
        playerTwo.pullCard(10);
        playerOne.pullCard(10);
    }



    public boolean gameEndsInDraw(){
        if(playerOne.getWins()==3&&playerTwo.getWins()==3){
            return true;
        }
        return false;
    }

    public String playerString(){
        return
                    "PlayerOne: " + playerOne.getName() + "\n"
                +   "Wins: "+ playerOne.getWins() + "\n"
                +   "Pass: "+ playerOne.isPass()  + "\n"
                +   "PlayerTwo: " + playerTwo.getName() + "\n"
                +   "Wins : "+ playerTwo.getWins() + "\n"
                +   "Pass:  "+ playerTwo.isPass();
    }

    public void print(){
        System.out.println(playerString());
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(Player playerOne) {
        this.playerOne = playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(Player playerTwo) {
        this.playerTwo = playerTwo;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }
}
