package Enviroment.Controller.ConsoleController;


import Enviroment.Model.Player.Player;
import enums.Raw;

public class PlayerController {
    private Player player;
    public PlayerController() {
    }


    public boolean checkForCard(String cardName){
       return player.getTable().checkIfCardIsInHand(player.getName(),cardName);
    }

    public boolean buffOrDebuffCard(String cardname){return player.getTable().getOwnField(player.getName()).getHand().buffOrDebuffCard(cardname);}

    public void passRound() {
        player.passRound();
    }



    public boolean didIPass(){
        return player.isPass();
    }


    public void useCard(String cardName) {
        player.placeCard(cardName);
    }

    public void useCard(String cardName, Raw raw) {
        player.placeCard(cardName,raw);
    }

    public boolean isMyTurn(){
        return player.isTurn();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
