import Agent.Agent;
import Agent.HumanAgent;
import Agent.MoreComplexAgents.ComplexAgent;
import Agent.RandomAgent.DumbRandomAgent;
import Agent.RandomAgent.SmartRandomAgent;
import Agent.RuleAgent.CardBasedAgent.CardHoldOutAgent;
import Agent.RuleAgent.CardBasedAgent.SmartCardHoldOutAgent;
import Agent.RuleAgent.Dumb.DumbRuleAgent;
import Agent.RuleAgent.Dumb.DumbRuleAgentWithMagicCards;
import Enviroment.Model.Game.MainSystem;
import Enviroment.View.Frame;

import javax.swing.text.View;
import java.util.Scanner;

public class Main {


    public static void main (String []args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        String input;
       /* System.out.println("Spiel mit Frame y/n");
        input = scanner.next();
        while (true){
            if(input.toLowerCase().equals("y")){
                break;
            }
            if(input.toLowerCase().equals("n")){
                break;
            }
            System.out.println("Falsche Eingabe. Bitte y für Ja und n für Nein");
            input = scanner.next();
        }
        if(input.equals("y")){
            Frame frame = null;
            MainSystem mainSystem = new MainSystem(frame,new ComplexAgent());

            mainSystem.startHumanGame();

        }else*/{
        System.out.println("Wollen sie ein Automatisches Spiel spielen? y/n");
        input = scanner.next();
        while (true){
            if(input.toLowerCase().equals("y")){
                break;
            }
            if(input.toLowerCase().equals("n")){
                break;
            }
            System.out.println("Falsche Eingabe. Bitte y für Ja und n für Nein");
            input = scanner.next();
        }
        if(input.toLowerCase().equals("y")){

            System.out.println("DebugGame ? y/n");
            input=scanner.next();
            while (true){
                if(input.toLowerCase().equals("y")){
                    break;
                }
                if(input.toLowerCase().equals("n")){
                    break;
                }
                System.out.println("Falsche Eingabe. Bitte y für Ja und n für Nein");
                input = scanner.next();
            }
            if(input.toLowerCase().equals("n")) {
                Agent agents[] = {new CardHoldOutAgent(),new DumbRuleAgentWithMagicCards(),new DumbRandomAgent(), new SmartRandomAgent(), new DumbRuleAgent()};
                AutomaticGames automaticGames = new AutomaticGames(agents);
                // Hier den Agent ändern
                automaticGames.startGameSequences(new ComplexAgent(), 1000000);
            }else{
                // Debug Game
                MainSystem mainSystem = new MainSystem(new CardHoldOutAgent(),new ComplexAgent());
                mainSystem.startGameDebug();
            }

        }else {
            MainSystem mainSystem = new MainSystem(new HumanAgent(),new ComplexAgent());
            mainSystem.startHumanGame();
        }



    }
    }
}
