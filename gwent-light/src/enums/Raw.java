package enums;

public enum Raw {
    RAW_ONE (0),
    RAW_TWO(1),
    RAW_THREE(2);

    private final  int rawNumber;

    private Raw(int rawNumber){

        this.rawNumber = rawNumber;
    }
}
