package enums;

public enum CardType {
        BUFFCARD,
        MONSTERCARD,
        ERASECARD,
        PULLMORECARD,
        DEBUFFCARD,
        ANTIDEBUFFCARD
}


