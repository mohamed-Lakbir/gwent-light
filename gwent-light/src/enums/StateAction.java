package enums;

public enum StateAction {
    STRONG_MONSTER,
    MEDIUM_STRONG_MONSTER,
    MEDIUM_WEAK_MONSTER,
    ERASE,
    BUFF,
    DEBUFF,
    ANTIDEBUFF,
    PASS
}
